#! /usr/bin/env bash

ROOT=$(dirname "$(dirname "$0")")
DEVENV_ROOT=$(realpath "$ROOT/_devenv")
PKI_ROOT=$(realpath "$DEVENV_ROOT/pki")
EASYRSA_ROOT=$(realpath "$DEVENV_ROOT/easy-rsa")

if [[ ! -d "$EASYRSA_ROOT" ]]; then
  git clone https://github.com/OpenVPN/easy-rsa "$EASYRSA_ROOT"
fi

pushd "$EASYRSA_ROOT" || exit 1
git checkout v3.0.6
popd || exit 1

if [[ -d "$PKI_ROOT" ]]; then
  echo "!!! Can't recreate PKI. If you really mean it, remove the '${PKI_ROOT}' directory."
  exit 1
fi

EASYRSA=$(realpath "$EASYRSA_ROOT/easyrsa3/easyrsa")
echo "easyrsa path: $EASYRSA"

EASYRSA_BATCH=1
export EASYRSA_BATCH

EASYRSA_REQ_CN="Local Dev CA for $(hostname) @ ${ROOT}"
export EASYRSA_REQ_CN

pushd "$DEVENV_ROOT" || exit 1
"$EASYRSA" init-pki

"$EASYRSA" build-ca nopass

"$EASYRSA" --subject-alt-name="DNS:\*.127.0.0.1.xip.io,DNS:127.0.0.1.xip.io" build-server-full '127.0.0.1.xip.io' nopass

openssl rsa -in pki/private/127.0.0.1.xip.io.key -out pki/private/127.0.0.1.xip.io.key

cp pki/private/127.0.0.1.xip.io.key containers/nginx-front-door/xipio-wildcard.key
cat pki/issued/127.0.0.1.xip.io.crt pki/ca.crt > containers/nginx-front-door/xipio-wildcard.cert

CERT_PATH=$(realpath "$PKI_ROOT/ca.crt")
popd || exit 1

if [[ $(uname) == 'Linux' ]]; then
  HASHED_CERT_NAME="dev-cert-$(echo "${CERT_ROOT}" | sha256sum | cut -c1-6).crt"

  echo "!!! For most Linux distros, run the following command to load your dev cert"
  echo "    into your OS trust store:"
  echo
  echo "sudo bash -c 'cp \"${CERT_PATH}\" /etc/pki/ca-trust/source/anchors/${HASHED_CERT_NAME} && update-ca-trust'"
elif [[ $(uname) == 'Darwin' ]]; then
  echo "!!! For a Mac, run the following command to load your dev cert into the trust store:"
  echo
  echo "sudo security add-trusted-cert -k /Library/Keychains/System.keychain -d \"${CERT_PATH}\""
else
  echo "!!! Unrecognized OS: '$(uname)'. Your build environment is probably fine,"
  echo "    but you will need to load the following locally-generated cert into"
  echo "    your trust store(s) manually:"
  echo
  echo "${CERT_PATH}"
fi

echo ""
echo "You'll also need to trust your certs in your browser. The cert's base"
echo "path is '${CERT_PATH}'."
echo
echo "Firefox: Preferences -> Certificates -> View Certificates -> Import"
echo "Chrome:  TBD"
echo "Safari:  TBD"
echo ""

