#!/usr/bin/bash

ROOT=$(realpath "$1")
for link in $(find "$ROOT" -type l | grep -v '.bin'); do
  target="$(readlink -e "$link")"

  echo "${link} -> ${target}"

  rm "$link"
  cp -R "$target" "$link"
done
