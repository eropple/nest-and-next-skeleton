#! /usr/bin/env bash
# Docker for Mac and Docker for Windows (not that we really care
# about supporting that) support the `host.docker.internal` DNS
# entry to refer back to an IP address that points to the host,
# where we'll be running our development version of the application.
# This is necessary to route requests through our nginx "front door",
# which lets us put a TLS cert on `www.127.0.0.1.xip.io` and on
# `api.127.0.0.1.xip.io` that we can then use for HTTPS requests
# going forward, all inside our own local dev environment. It keeps
# that local dev environment looking as much like production as
# possible.
#
# This script should be `source`d before running any Docker Compose
# commands.

set -euo pipefail

if [[ "$WSL2" == '1' ]]; then
  HOST_ADDR=$(ip -4 addr show scope global dev eth0 | grep inet | awk '{print $2}' | cut -d / -f 1)
elif [[ $(uname) == 'Linux' ]]; then
  HOST_ADDR=$(ip -4 addr show scope global dev docker0 | grep inet | awk '{print $2}' | cut -d / -f 1)
else
  HOST_ADDR=host.docker.internal
fi

echo " - Docker host addr: ${HOST_ADDR}"
export HOST_ADDR
