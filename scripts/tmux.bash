#! /usr/bin/env bash

ROOT="$(dirname "$(dirname "$0")")"

cd "$ROOT" || exit 1

[[ -f "$ROOT/backend/.env" ]] && . "$ROOT/backend/.env"

tmux \
  start-server \; \
  set -g mouse on \; \
  new-session  "$SHELL" \; \
  split-window -h "yarn run -s dev:up; read" \; \
  split-window -v -p 80 "cd backend && yarn run -s start:dev; read" \; \
  split-window -h "yarn exec -s -- nodemon -q -e tsbuildinfo -w ./backend/dist --exec 'yarn run -s openapi'; read" \; \
  split-window -v "cd frontend && yarn run -s start:dev; read" \;
  # select-layout tiled
