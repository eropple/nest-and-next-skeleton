FROM node:12-alpine
RUN apk add jq bash --no-cache && npm install -g yarn

# This pre-caches dependencies so we can do less work later. This step will only
# change if your `yarn.lock` or package.json files change, which helps us minimize
# re-run steps.
#
# If you add additional packages to this workspace, you will want to copy their
# package.json files in this step. (Make sure their directories exist, of course.)
RUN mkdir -p /build
RUN mkdir -p /build/backend
COPY ./package.json /build/package.json
COPY ./backend/package.json /build/backend/package.json
COPY ./yarn.lock /build/yarn.lock

WORKDIR /build
RUN yarn install

COPY . /build/
WORKDIR /build
RUN yarn install