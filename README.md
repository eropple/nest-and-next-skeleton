# How to Use #
This project is intended to be an example of what I consider to be good, smart practices for the use of NestJS as a backend API and NextJS to provide a fast-rendering frontend. And, since I needed to make it for my own use, I figured there's no harm in making it available for anyone else who'd like to take a look. This document includes notes--hopefully ones useful to people other than me, though they are in many ways intended to be reminders for Future Me, who I try really hard to be good to. They're probably not sufficiently in order; PRs to improve my scribbles shall be gratefully received.

Some of the opinions that inform this skeleton project are pretty heterodox; in particular, a few decisions actively don't leverage systems recommended in the NestJS API docs (more on that later). But I've found significant success in leveraging a set of open-source libraries--some I've written, some I've found in my travels--to create a straightforward skeleton within which one can write a SaaS project. **One important warning as you start to explore this project:** while I think it's generally straightforward, it's really important to understand that this is not a "five minute demo" kind of boilerplate project. A lot of the decisions made in here are opinionated and pretty picky and while I've made what I think are sober choices, there's an expectation that you either already have a strong command of full-stack web application development or are very eager to learn.

## Major Components ##
- The `backend` directory includes the NestJS application. It also includes a command line interface, exposed via `yarn run cli`, that is used for administration. (Administrative web interfaces are not my jam, and I don't build them unless required; writing CLI endpoints via `clime` is more my thing.)
- The `frontend` directory is a NextJS/React/Redux application set up with `redux-storage` to persist anything under the `persistent` key in the Redux store. It's rigged to use `redux-thunk` (I'm not a fan of `redux-saga` but you could swap it out without really trying too hard.)
- The `api-clients` directory contains automatically generated API clients. Or it can contain clients, plural, if you really want it to--right now it includes only the Typescript/Axios client used by the `frontend`. You can generate these clients from the root of the repository with `yarn openapi`.
- There is a `docker-compose.yml` file in the root directory running a dev database, a test database, an instance of Mailcatcher to act as a local SMTP server, and the nginx "front door" (more on that in a bit).

## Supported Development Platforms ##
I'm a Linux user at work, almost exclusively Fedora; this should work on any reasonably recognizable Linux setup. I occasionally test on Macs; you might need some compatibility software like the GNU coreutils, and I won't document those needs (though someone else is welcome to).

A Unix-based OS, however, _is_ necessary. For example, `yarn test:e2e` sources `test.env` using a bash/zsh syntax. I'm not going to change this. Windows has WSL now, after all (and most of this skeleton works with WSL2!).

You will also need a reasonably recent JVM installed to generate `api-clients`. The OpenAPI folks love them some Java-based codegen, though the actual codegen tool is packaged via NPM to make our lives a little easier.

To use the containerization features included in this skeleton, which use multi-stage builds to create smaller end containers, you need at least Docker 17.05. In dev, you should probably track the newest stable.

## Enough Talk--How Do I Run It? ##
**BEFORE YOU DO ANYTHING ELSE:** Run `./scripts/build-dev-pki.bash` and follow its instructions. This is important, because our dev environment is HTTPS all the way through and you'll rapidly grow frustrated if you aren't trusting your dev certificate.

The easiest way is then to kick it off from the root of the repository. Just do a `yarn install` and then `yarn dev:start` to 1) start up docker-compose, 2) start the backend, 3) start the watcher that rebuilds the frontend's API client, and 4) start the frontend.

We use [tmux]() to achieve this wizardry. tmux might be a new one to you, but it's a pretty time-tested tool. If you are not by habit a shell-heavy developer, this gizmo might feel overwhelming, but give it a try--it's a nicer experience than manually running a bunch of stuff in a bunch of different windows. I've preconfigured it such that it shows all of the four above items in four equally-spaced panes in your terminal; I then use another tab (mine are _in_ tmux, but you can also just use the tabs in your terminal emulator) for any manual interactions I have to perform.

There are a lot of things you can do with tmux. The only thing you need to know _immediately_ is that hammering `Ctrl-C` a bunch will get you out of it. For more, you should read the fantastic [this tmux cheat sheet](https://gist.github.com/andreyvit/2921703).

# Routing and the "Front Door" #
This project is structured so that, when you start the dev environment via `yarn dev:start`, an nginx instance called the "front door" will be started. It handles application routing throughout your stack, making sure requests go to the frontend and backend respectively. It does create for you the following assertions:

- Your frontend and backend should be served on the same domain. In development, this will be something like `TENANT_NAME.127.0.0.1.xip.io` (more on tenants later). You can more or less copy (with some security-minded modifications) the `nginx.conf` file at `_devenv/containers/nginx-front-door/nginx.conf` to understand the core structure for production. Or you can use something like an AWS Application Load Balancer to serve your stuff.
- The API lives at `https://TENANT_NAME.127.0.0.1.xip.io/api`.
- API docs live at `https://TENANT_NAME.127.0.0.1.xip.io/api-docs`.
- Your OpenAPI spec lives at `https://TENANT_NAME.127.0.0.1.xip.io/openapi.json`.

You'll rarely if ever actually hit the local dev port for your backend API or your frontend web app instances, though they _will_ run on your local machine for ease of development.

# Backend Stuff #
## Configuration ##
Basically everything is configured through environment variables. For the backend, these config options are defined at module level via an interface or a `runtypes` record (usually--there are some exceptions, like TypeORM bringing its own config format) and defined in terms of environment variables in the `backend/src/_cfg` directory.

You can change a number of foundational aspects of the application, like the application name (used in logs) or the prefix for your environment variables, by modifying `backend/src/cfg/_base.ts`.

The included `dev.env` file includes developer-friendly settings for more or less everything, and if you copy that file to `.env` your app will pick it up and it'll be easy to get started. Similarly, the included `test.env` file includes the necessary variables to run tests using the provided `docker-compose.yml` file to stand up a test database.

Obviously, you should not use `dev.env` settings in production. That would be disastrous.

## Testing ##
### Unit Tests ###
I'll be honest: I'm not big on unit tests for general web plumbing. They have a place when working on business logic, but there is really very little business logic anywhere in this skeleton. However, there's real value to integration tests, so let's talk about...

### E2E Tests ###
#### The `TestHarness` ####
The out-of-the-box NestJS E2E testing relies on a lot of boilerplate. I _hate_ boilerplate, and I've encapsulated as much of it as I could into the `TestHarness` class. This class will handle application initialization, `supertest`, and setting up requests for authenticated users.

#### Data and Fixtures ####
This project takes on something of a "Rails-y" approach to tests. And that approach is to nuke the database from orbit on every run. As such, the `docker-compose.yml` file spins up _two_ databases so that you can be assured that your tests won't stomp on your dev database when you run them.

#### API Activity ####
For now, you should use `supertest` to test your APIs, as per a normal NestJS project. _However_, it would be great if we could use an OpenAPI client to test our APIs! Not sure how to do that, though. Please see _Opportunities to Contribute_ at the bottom for more.

## Tenancy ##
The first, and most annoying, boil on the rear end of most projects that I've ever worked on is tenancy. Many applications I've worked on have grown to the point where they need some kind of scoping beyond a single user, whether it's for white-labeling or because each purchaser needs to be isolated from the others. I've learned over time that--since I've already written it once--it's easier to start a project with the notion of tenancy in place and just have a single global tenant until I need more.

The use of a global tenant adds all of one unique-key lookup to a request. If the performance impact of doing one extra database lookup is going to kill you, this project skeleton is not for you.

### Naming Tenants ###
You could do this a lot of ways. Please let me save you all the rake-stepping I've done. _Name your clients after their frontend canonical hostnames._ If you're hosting a bunch of clients off your domains, make your tenants `bobcorp.example.com`, `foocorp.example.com`, `ed-test.dev.example.com`, that sort of thing. Include the fully qualified domain--it will make life easier if (when (you hope!)) somebody with a big fat NASDAQ ticker symbol wants you to host `exampleapp.hugeco.com` for them.

Even if you're just using a global tenant, I still recommend this scheme. In prod, do `example.com`; in dev, do `dev.example.com`; in local-dev, do `127.0.0.1.xip.io` (if you don't know xip.io, check 'em out, they're great). It keeps you in good shape for the future and has the handy side effect of making it harder to fat-finger something; if you name your tenant `global` and then you run `yarn cli do-something-destructive --tenant-name global`, you get the wonderful feeling of wondering "did I just run that against dev or prod?" all to yourself.

Build smart firebreaks into your systems. Name things clearly. The `frontend` project already expects this; `www.example.com` turns into `example.com`, `www.127.0.0.1.xip.io` turns into `127.0.0.1.xip.io`, `orange.127.0.0.1.xip.io` stays `orange.127.0.0.1.xip.io`.

### Creating Tenants ###
I've added a CLI command to create tenants to get you started, as well as to make for a decent example command for you to follow when adding your own. Since we use `clime`, you get a pretty explorable command line in general, but you can get details via:

```
yarn run -s cli tenant add-tenant --help
yarn run -s cli tenant add-tenant 127.0.0.1.xip.io
```

It's not rocket science.

## Users, Authn, and Authz ##
_This all assumes familiarity with [@eropple/nestjs-auth](https://github.com/eropple/nestjs-auth). The best way to get that familiarity is probably to check out [nestjs-auth-example](https://github.com/eropple/nestjs-auth-example)._

Authentication and authorization are handled as part of this skeleton; one could say _you can't get away from it if you tried_. The type system, in `backend/src/authx/identity-types.ts`, is set up to recognize only two types of principal: a `User` and an `InternalService`. Internal services are just a stub; more on that later.

### Adding a User ###
This example intentionally does not provide an API endpoint for creating users. If you want this for your application, you should write it and properly secure it; for public-signup APIs you may want to consider email verification and for privately-managed tenants you will probably want to create an administrator role for that tenant and create signup endpoints restricted to those users. There is a CLI command that you can run to create a user:

```
yarn run -s cli user create-user --help
yarn run -s cli user create-user 127.0.0.1.xip.io ed ed@example.com
```

### Authz Seems Useless? ###
The kind of authz used in this project, of the scopes/grants/rights variety, are another one of those things that feels like overkill until you need it--upon which time you're trying to retrofit something on and hoping you catch all your edge cases. I've found that the rights tree approach of `@eropple/nestjs-auth` is one that I end up reimplementing in an _ad hoc_ way when I don't have it, even before the need of grants and scopes--and with this library, a grant of `**/*` and some lightly considered scopes makes its use low-impact while making me at least _think_ about security in such a way as to not box myself into a corner later.

### Internal Services Don't Actually Work In This Skeleton ###
The idea behind an `InternalService` principal is to give you a way to model systems that you trust, but are outside the process boundary of your API. However, the Internal services are only stubbed out to give you an idea of the power and flexibility of the auth system; the authx provider recognizes an internal service header but if one is detected we just throw `NotImplementedException`.

There are a few good ways to implement those. My go-to is an internal CA with client TLS certificates, using nginx to unwrap them and pass them to my API as headers, but I've also implemented a shared-secret system where I used AWS IAM policies to control access to the shared secret. Regardless, unless somebody can suggest a dead-simple, easy-to-manage, secure way to implement this that any downstream implementor can be reasonably expected to use, we'll leave the actual implementation outside the scope of this project.

### 2FA ###
In 2019, your app should support some kind of two-factor authentication. At minimum, you should support TOTP (Google Authenticator/Authy codes). It'd be _nice_ to implement WebAuthN, but I have yet to find a really good pattern for doing so. PRs gratefully accepted.

In order to avoid the situation where we enable TOTP for a user who fails to set up their authenticator correctly, we require TOTP verification before it will be demanded as part of login. When we enable TOTP, we return the TOTP secret to the user; at that point it is incumbent upon the user to generate a six-digit TOTP code and pass it back to us. Once they've done this, all subsequent logins will require a TOTP code to be presented as part of login.

We don't support paper keys as fallbacks. I tend to think that's a customer service facility, so it's not a focus for me. PRs gratefully accepted if other folks would like to implement it, though.

## Databases ##
This skeleton uses Postgres. It's 2020. You should use Postgres unless you know exactly why you should not use Postgres. (Few people do. But then, few people should use something that isn't Postgres.)

### Readonly Fields in Entities ###
Entities in the codebase are liberally sprinkled with `readonly` fields. In my experience this is not _common_ in TypeORM but I find it really helpful to be able to denote that, no, these fields shouldn't be modified by consumers--either _at all_, in the case of something like `id`, or in the context of the application, such as `updatedAt`, see below. I recommend you do the same--if a field cannot be modified without things getting weird, make sure that it _can't_ be.

### `updatedAt`? ###
TypeORM supports `@UpdateDateColumn()`. I don't use it and you shouldn't either. Instead I apply a trigger--defined in `src/db/migration_helpers`, and designed to be consumed by your migrations--that updates the column on any modification to the row, whether it's through TypeORM or anything else. This is why `updatedAt` is a readonly field on every entity that is not intended to be deleted rather than updated: because _you_ should never be setting it.

## Mail ##
SMTP-based mail sending is implemented using [@eropple/nestjs-rx-mailer](https://github.com/eropple/nestjs-rx-mailer), which itself consumes [@eropple/rx-mailer](https://github.com/eropple/rx-mailer). rx-mailer is a really simple--properly simple, IMO--library that allows you to create HTML emails by using React. I've never liked having to context switch away from whatever programming language I'm using into Handlebars or whatever just to write emails, and we're using React with NextJS anyway, so we just _should_ be able to write our emails with React too.

You can see an example of a really simple email in `ping.controller.ts`. There it uses `React.createElement` to instantiate the `TestEmail` class. If you'd like, you 100% can make your controllers `.tsx` files and use inline JSX; I'm not sure how I feel about that, so I haven't done it here, but there's nothing stopping you.

Since rx-mailer is built on top of [Nodemailer](https://nodemailer.com), you have a number of options for sending email, including SMTP, sendmail (ew) and Amazon SES (not bad!). This skeleton defaults to SMTP, sending it to Mailcatcher (an accept-all mail service running as part of the docker-compose file); it sends to port `21025` and you can go see all sent emails at `http://localhost:21080`.

You can also send file attachments, include plaintext versions of your emails (you should do this!), and more, through the rx-mailer API. Good thing it's TypeScript and it's easy to explore, huh?

## Error Handling and Reporting ##
This project defines an error filter (`backend/src/error.filter.ts`) that computes and writes an `ErrorDTO` to the client. This `ErrorDTO` is the default fallback type for all responses except for `400 Bad Request`, as this is used primarily by the OpenAPI middleware to validate parameters. (If you throw a 400 Bad Request `HttpException`, it will be emitted as a `BadRequestDTO`, as will any created by `OpenapiModule`.)

This project does _not_ define any particular way to report errors to a central system--Sentry, Bugsnag, that sort of thing. I wanted to do it, but at present I'm a little unconvinced about the security of `nest-raven` and I've proposed significant API changes that would necessitate breaking this project. No bueno, so let's hold off for a bit.

# Frontend Stuff #

# Packaging #
Because we are maximally bloggable, we use Docker containers as our deployable. If you're in ECS or Fargate or if you want to be a real president-saving Bad Dude you can use Kubernetes for this; your deployments are outside the scope of this project.

We have some Annoying Constraints, though, to get around the difficulty of using a Yarn workspace with internal references (like the way that the frontend relies upon `@app-clients/typescript-axios`). Yarn workspaces symlink shared packages, so we have to get a little bit clever with the way that we build our containers so that 1) we're building as little as possible at any step, and 2) we're not accidentally _not_ building something we rely upon. To this end we use multi-stage build packages, doing a bunch of work up front in order to minimize the work we do on every build or code change. To this end I've built these containers to be good for _deployment_ (size, speed of startup), but not necessarily for _development_; if you're the sort of developer who wants to do your development in Docker containers--well, I think that's silly, but I'll happily accept some PRs.

You can kick off the entire build process with `yarn package` from the root of the repository or specifically build one or the other with `yarn docker:backend` or `yarn docker:frontend`. Unless you customize your root `package.json`, it'll build containers `app-backend` and `app-frontend`, tagging it with the current git tag, current git branch, and `latest`. (If you have a dirty repository, the first two will be of the form `aabbccd-dirty` and `mybranch-dirty`.)

# Opportunities To Contribute #
This skeleton is pretty cool (kinda like [Shaun](https://www.youtube.com/channel/UCJ6o36XL0CpYb6U5dNBiXHQ)) but it's incomplete. Here's a list of things that would be nice additions and would probably help folks out:

- We're generating all this OpenAPI stuff - we should figure out a way to generate an OpenAPI client that works with `supertest`. Anybody have any ideas?
- Extract reusable tooling into its own packages/frameworks:
  - The `envOrFail`, `envBoolean`, etc. stuff doesn't really rely on anything else in the codebase.
  - Maybe there's a good way to marry `clime` and NestJS instead of just bashing them together. (This one may be complex; we don't _always_ build just a CLI context, as in the case of our OpenAPI doc printer.)
- WebAuthN support (Yubikeys, etc.) -- the codebase is structured such that they shouldn't be too awful to include.
- Docker stuff:
  - Container builds are a little slow, though we're trying to improve it with memoized files and multi-stage builds; any ideas on how to improve it?
  - Document the adding of native dependencies to the Alpine-based base and application images.
- Out-of-the-box error reporting to Sentry or to another self-hostable, open-source error reporting platform.
