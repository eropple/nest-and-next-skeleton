FROM app-base:latest AS backend-builder
WORKDIR /build/backend
RUN yarn build

# This will reinstall modules _without_ including stuff like `typescript`, `jest`,
# and your typings in your finalized container.
RUN rm -rf node_modules && yarn install --production

# This handles the situation where you're dealing with packages that are
# symlinked (such as other packages in your Yarn workspace) and copies them
# into place. It is a somewhat resource-inefficient operation, but as we
# ship the second package and not this one, it's not a huge deal.
RUN bash /build/scripts/flatten-symlinks.bash /build/backend

## ------------------------------------------------------------------------- ##

FROM node:12-alpine
COPY --from=backend-builder /build/backend/ /app/
WORKDIR /app

# PLEASE NOTE: you might need to change this, particularly if you're using a
#              cloud provider and need to source configuration from other
#              sources, like AWS Secrets Manager or the like. I typically end
#              up with a launcher script that sets my env vars (as it's unsafe
#              to do so in Terraform etc., it allows users who can see your
#              state to acquire secrets even if they lack permissions to
#              otherwise do so!) and then launched the app.
CMD yarn run -s start:prod