import { Ctor } from '@eropple/nestjs-openapi3';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';

import { configureApp } from '../../src/app-initialization';
import { AppModule } from '../../src/app.module';
import { Tenant } from '../../src/directory/db/Tenant.entity';
import { User } from '../../src/directory/db/User.entity';
import { TenantService } from '../../src/directory/tenant.service';
import { UserService } from '../../src/directory/user.service';

let tenantCount = 0;
let userCount = 0;

export class TestHarness {
  private _app!: INestApplication;
  private _request!: request.SuperTest<request.Test>;

  get app(): INestApplication {
    if (!this._app) {
      throw new Error('Cannot access app before initialization.');
    }

    return this._app;
  }

  get request(): request.SuperTest<request.Test> {
    if (!this._request) {
      throw new Error('Cannot access supertest before initialization.');
    }

    return this._request;
  }

  tenantService!: TenantService;
  userService!: UserService;

  private constructor() {

  }

  get<T>(type: Ctor<T> | string): T {
    return this._app.get(type);
  }

  authedGet(path: string, token: string) {
    return this._request.get(path).set('authorization', token);
  }

  authedPost(path: string, token: string) {
    return this._request.post(path).set('authorization', token);
  }

  authedPut(path: string, token: string) {
    return this._request.put(path).set('authorization', token);
  }

  authedDelete(path: string, token: string) {
    return this._request.delete(path).set('authorization', token);
  }

  async withTenant<T>(fn: (tenant: Tenant) => Promise<T>): Promise<T> {
    const tenantName = `test-tenant-${tenantCount.toString().padStart(4, '0')}.example.com`;
    const newTenant = await this.tenantService.createTenant({
      name: tenantName,
      displayName: tenantName,
    });

    tenantCount++;

    return fn(newTenant);
  }

  async withUser<T>(tenant: Tenant, fn: (user: User, password: string) => Promise<T>): Promise<T> {
    const userServiceForTenant = await this.userService.forTenant(tenant);

    const userNum = userCount.toString().padStart(4, '0');
    const [newUser] = await userServiceForTenant.createUser(`u-${userNum}`, `u-${userNum}@example.com`);

    const userServiceForUserInTenant = await userServiceForTenant.forUser(newUser);
    const [user, password] = await userServiceForUserInTenant.resetPassword();

    userCount++;

    return fn(user, password);
  }

  async withTenantAndUser<T>(fn: (tenant: Tenant, user: User, password: string) => Promise<T>): Promise<T> {
    return this.withTenant(async t => this.withUser(t, async (u, p) => fn(t, u, p)));
  }

  private async initialize() {
    await this.initializeApp();
    await this.initializeServices();
    await this.initializeRequest();
  }

  private async initializeApp() {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    const app = moduleFixture.createNestApplication();
    await configureApp(app);
    await app.init();

    this._app = app;
  }

  private async initializeServices() {
    this.tenantService = this.get<TenantService>(TenantService.SINGLETON_TOKEN);
    this.userService = this.get<UserService>(UserService.SINGLETON_TOKEN);
  }

  private async initializeRequest() {
    this._request = request(this._app.getHttpServer());
  }

  static async build(): Promise<TestHarness> {
    const harness = new TestHarness();
    await harness.initialize();

    return harness;
  }
}
