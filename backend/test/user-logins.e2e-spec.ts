import * as Speakeasy from 'speakeasy';

import { USER_COOKIE_NAME } from '../src/authx/constants';
import { PingResponse } from '../src/ping.controller';
import { LoginUnPwFailedResponse, LoginUnPwSuccessfulResponse } from '../src/session/domain';
import { TestHarness } from './helpers/TestHarness';

describe('User logins (e2e)', () => {
  let test: TestHarness;
  beforeAll(async () => { test = await TestHarness.build(); });
  afterAll(async () => {
    if (test) {
      await test.app.close();
    }
  });

  it('should disallow bad-password logins', async () => {
    await test.withTenantAndUser(async (tenant, user, passwordPlaintext) => {
      const failResponse: LoginUnPwFailedResponse =
      (await test.request
        .post(`/api/${tenant.name}/session/login/unpw`)
        .send({
          identity: user.email,
          passwordPlaintext: 'my evil bit is set as per RFC 3514.',
        })
        .expect(403)
      ).body;

      expect(failResponse.success).toBe(false);
      expect(failResponse.reason).toBe('bad-credentials');
    });
  });

  it('should allow a passworded login', async () => {
    await test.withTenantAndUser(async (tenant, user, passwordPlaintext) => {
      const loginResponse: LoginUnPwSuccessfulResponse =
        (await test.request
          .post(`/api/${tenant.name}/session/login/unpw`)
          .send({
            identity: user.email,
            passwordPlaintext,
          })
          .expect(200)
        ).body;

      expect(loginResponse.user.name).toBe(user.name);

      const headerPingResponse: PingResponse =
        (await test.authedGet('/api/ping/authed', loginResponse.token)
          .expect(200)
        ).body;

      const cookiePingResponse: PingResponse =
        (await test.request.get('/api/ping/authed')
          .set('Cookie', [`${USER_COOKIE_NAME}=${loginResponse.token}`])
          .expect(200)
        ).body;
    });
  });

  it('should reject a TOTP-enabled login w/o totp code', async () => {
    await test.withTenantAndUser(async (tenant, user, passwordPlaintext) => {
      const usersInTenant = await test.userService.forUserInTenant(tenant, user);
      const totpSecret = (await usersInTenant.setTOTP()).secret;

      const totpCode = Speakeasy.totp({ secret: totpSecret, encoding: 'base32' });
      await usersInTenant.validateTOTP(totpCode);

      const failResponse: LoginUnPwFailedResponse =
        (await test.request
          .post(`/api/${tenant.name}/session/login/unpw`)
          .send({
            identity: user.email,
            passwordPlaintext,
          })
          .expect(403)
        ).body;

      expect(failResponse.success).toBe(false);
      expect(failResponse.reason).toBe('totp-required');
    });
  });

  it('should reject a TOTP-enabled login with bad totp code', async () => {
    await test.withTenantAndUser(async (tenant, user, passwordPlaintext) => {
      const usersInTenant = await test.userService.forUserInTenant(tenant, user);
      const totpSecret = (await usersInTenant.setTOTP()).secret;

      const totpCode = Speakeasy.totp({ secret: totpSecret, encoding: 'base32' });
      await usersInTenant.validateTOTP(totpCode);

      const failResponse2: LoginUnPwFailedResponse =
        (await test.request
          .post(`/api/${tenant.name}/session/login/unpw`)
          .send({
            identity: user.email,
            passwordPlaintext,
            totpCode: '123123',
          })
          .expect(403)
        ).body;

      expect(failResponse2.success).toBe(false);
      expect(failResponse2.reason).toBe('bad-totp');
    });
  });

  it('should allow a passworded login + totp', async () => {
    await test.withTenantAndUser(async (tenant, user, passwordPlaintext) => {
      const usersInTenant = await test.userService.forUserInTenant(tenant, user);
      const totpSecret = (await usersInTenant.setTOTP()).secret;

      const totpCode = Speakeasy.totp({ secret: totpSecret, encoding: 'base32' });
      await usersInTenant.validateTOTP(totpCode);

      const loginResponse: LoginUnPwSuccessfulResponse =
        (await test.request
          .post(`/api/${tenant.name}/session/login/unpw`)
          .send({
            identity: user.email,
            passwordPlaintext,
            totpCode,
          })
          .expect(200)
        ).body;

      expect(loginResponse.user.name).toBe(user.name);

      const pingResponse: PingResponse =
        (await test.authedGet('/api/ping/authed', loginResponse.token)
          .expect(200)
        ).body;
    });
  }, 60000);
});
