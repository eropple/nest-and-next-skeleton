import request from 'supertest';
import { TestHarness } from './helpers/TestHarness';

describe('Ping (e2e)', () => {
  let test: TestHarness;
  beforeAll(async () => { test = await TestHarness.build(); });
  afterAll(async () => {
    if (test) {
      await test.app.close();
    }
  });

  it('/ping/any (GET)', async () =>
    test.request
      .get('/api/ping/any')
      .expect(200)
      .expect({ ping: 'Ping!', authed: false }));
});
