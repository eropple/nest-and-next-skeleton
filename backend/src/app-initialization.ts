import { Bunyan, BunyanLoggerService, CorrelationIdMiddleware, ROOT_LOGGER } from '@eropple/nestjs-bunyan';
import { OpenapiModule } from '@eropple/nestjs-openapi3';
import { INestApplication, INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Request as ExpressRequest } from 'express';
import helmet from 'helmet';

import { API_URL, APPLICATION_NAME } from './_cfg/_base';
import { PACKAGE_VERSION } from './_cfg/_computed';
import { NEST_LOG_LEVEL, OPENAPI_LOG_LEVEL } from './_cfg/_log-levels';
import { RESPONSE_STACK_TRACES } from './_cfg/errors';
import rootLogger from './_cfg/root-logger';
import { AppModule } from './app.module';
import { BadRequestDTO, ErrorDTO, ErrorFilter } from './error.filter';

const openapiLogger = rootLogger.child({});
openapiLogger.level(OPENAPI_LOG_LEVEL);

export async function buildOpenapiDocument(app: INestApplication) {
  return OpenapiModule.createDocument(
    {
      app,
      baseLogger: openapiLogger,
      defaultResponses: {
        default: {
          description: 'An error or other unexpected condition occurred.',
          content: ErrorDTO,
        },
        400: {
          description: 'Something about the request was unprocessable or unusable.',
          content: BadRequestDTO,
        },
      },
    },
    (b) => {
      b.addTitle(`${APPLICATION_NAME} API`);
      b.addVersion(PACKAGE_VERSION);
      b.addServer({
        url: '/api',
      });
      b.addSecurityScheme('token', {
        name: 'Authorization',
        type: 'apiKey',
        in: 'header',
      });
    },
  );
}

/**
 * Builds a standard application (i.e., non-testing) for use
 * by the NestJS runner.
 */
export async function buildApp(): Promise<INestApplication> {
  const logger = rootLogger.child({});
  logger.level(NEST_LOG_LEVEL);
  const app = await NestFactory.create(AppModule, {
    logger: new BunyanLoggerService(logger),
  });

  return app;
}

export async function buildCli(): Promise<[Bunyan, INestApplicationContext]> {
  const logger = rootLogger.child({});
  logger.level(NEST_LOG_LEVEL);
  const ctx = await NestFactory.createApplicationContext(AppModule, {
    logger: new BunyanLoggerService(logger),
  });

  return [rootLogger, ctx];
}

/**
 * Statefully modifies the `INestApplication` with global-level
 * middlewares, guards, etc.
 *
 * This _should_ be used in testing.
 */
export async function configureApp(app: INestApplication) {
  /**
   * Since all development goes through the 'front door', we have a global
   * prefix attached to all endpoints.
   */
  app.setGlobalPrefix('/api');

  app.enableCors();
  /**
   * `helmet` sets a number of important security flags, including HSTS.
   */
  app.use(helmet());
  /**
   * `@eropple/nestjs-correlation-id` attaches a generated correlation ID
   * to every request, which is then exposed through the log layer.
   */
  app.use(CorrelationIdMiddleware());

  /**
   * Transforms returned errors into the `ErrorDTO` data type being offered as
   * a default response type, below.
   */
  app.useGlobalFilters(new ErrorFilter(rootLogger, RESPONSE_STACK_TRACES));

  /**
   * This enables OpenAPI 3.0 generation and the service thereof.
   */
  await OpenapiModule.attach(
    {
      app,
      baseLogger: openapiLogger,
      document: await buildOpenapiDocument(app),
      apiDocs: 'rapidoc',
      validationFailedResponse: (req: ExpressRequest, res, status, failures): BadRequestDTO => ({
        id: req.header('x-correlation-id') || 'X-CORRELATION-ID_NOT_FOUND',
        statusCode: status,
        failures,
      }),
    },
  );
}
