import { MailerOptions } from '@eropple/nestjs-rx-mailer';
import { Injectable } from '@nestjs/common';
import * as Bunyan from 'bunyan';
import * as TypeORM from 'typeorm';

import { AuthxConfig } from '../authx/config';
import { DirectoryConfig } from '../directory/config';
import { SessionConfig } from '../session/config';

import authx from '../_cfg/authx';
import directory from '../_cfg/directory';
import mailer from '../_cfg/mailer';
import rootLogger from '../_cfg/root-logger';
import session from '../_cfg/session';
import typeorm from '../_cfg/typeorm';

/**
 * There exist some more generic config management systems out there, like
 * `@nestjsx/config`. I do not recommend them; losing your type information
 * along the way, even if you assert a type later, isn't very good.
 *
 * If you _wanted_ to replace this with such a system I'd encourage you to use
 * `runtypes` to define your configuration values and use `Record#check` to
 * scream and fail when your configs are wrong. But that's a lot more work than
 * adding a config here and getting static typing all the way through (and thus
 * having it fail at compile time if there's a problem).
 */
@Injectable()
export class ConfigService {
  readonly rootLogger: Bunyan = rootLogger;
  readonly typeorm: TypeORM.ConnectionOptions = typeorm;
  readonly mailer: MailerOptions = mailer;

  readonly authx: AuthxConfig = authx;
  readonly directory: DirectoryConfig = directory;
  readonly session: SessionConfig = session;
}
