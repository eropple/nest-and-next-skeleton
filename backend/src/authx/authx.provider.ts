import { HttpAuthxInterceptor, PrincipalFn } from '@eropple/nestjs-auth';
import { Bunyan, InjectorKeys } from '@eropple/nestjs-bunyan';
import { NotImplementedException } from '@nestjs/common';
import { FactoryProvider, Scope } from '@nestjs/common/interfaces';
import { APP_INTERCEPTOR } from '@nestjs/core';
import _ from 'lodash';

import { ErrorDTO } from 'error.filter';
import { Tenant } from '../directory/db/Tenant.entity';
import { User } from '../directory/db/User.entity';
import { TenantService } from '../directory/tenant.service';
import { SessionService } from '../session/session.service';
import { AuthxConfig } from './config';
import {
  ANONYMOUS_SCOPES,
  INTERNAL_SERVICE_HEADER_NAME,
  USER_AUTH_HEADER_NAME,
  USER_COOKIE_NAME,
} from './constants';
import {
  AppIdentifiedBill,
  AppIdentityBill,
  AppRightsTree,
  Principal,
  UserBill,
} from './identity-types';

export const authx: FactoryProvider = {
  provide: APP_INTERCEPTOR,
  scope: Scope.REQUEST,
  inject: [InjectorKeys.LOGGER, 'AuthxConfig', TenantService, SessionService],
  useFactory: (
    baseLogger: Bunyan,
    cfg: AuthxConfig,
    tenants: TenantService,
    sessions: SessionService,
  ) => {
    const logger = baseLogger.child({ component: 'HttpAuthx' });
    logger.level(cfg.logLevel);

    const principalFn: PrincipalFn<AppIdentifiedBill<Principal>> = async (
      headers,
      cookies,
    ) => {
      const internalServiceHeader =
        _.flatten([headers[INTERNAL_SERVICE_HEADER_NAME]]).filter(v => v)[0];
      const userAuthHeader =
        _.flatten([headers[USER_AUTH_HEADER_NAME]]).filter(v => v)[0];
      const userAuthCookie =
        cookies[USER_COOKIE_NAME];

      if (internalServiceHeader) {
        logger.error('TODO: implement internal services.');
        throw new NotImplementedException();
      } else if (userAuthHeader || userAuthCookie) {
        const [ value, tokenSource ] =
          userAuthHeader ? [userAuthHeader, 'header'] : [userAuthCookie, 'cookie'];

        logger.debug({ tokenSource }, 'User token found; attempting to get token.');
        const token = await sessions.validateToken(value);
        if (!token) {
          logger.debug('Token is rejected.');
          return false;
        }

        logger.debug('Token verified - returning UserBill.');
        return new UserBill(token.user, token, token.grants);
      } else {
        logger.debug('No headers located; returning `null` for an AnonymousBill.');
        return null;
      }
    };

    const tree: AppRightsTree = {
      context: async (scopePart, req) => {
        const tenantName = req.params.tenantName;
        if (tenantName) {
          req.locals.tenant = await tenants.getTenantByName(tenantName);
        }
      },
      children: {
        // `always` is a helpful shorthand for "anything can hit this [public] endpoint".
        // We grant anonymous and identified users (both internal services and users) access
        // to those endpoints above, in their lists of scopes.
        always: {
          right: (_scopePart, req) => true,
        },
      },
      wildcard: {
        context: (_scopePart, req) => {
          const tenant: Tenant | null = req.locals.tenant;
          const identity = req.identity;

          // Anything inside the wildcard is implied to be scoped to a tenant. As such,
          // we want to return false (a 403 Forbidden) for any scope where a tenant doesn't
          // exist.
          if (!tenant) {
            return false;
          }

          if (identity.isIdentified) {
            const principal = identity.principal;

            // Similarly, we will assert that all users be members of the tenant they're
            // accessing. If you want to be more complex than this--well, I wouldn't, but
            // you have the tools.
            if (principal instanceof User && principal.tenant.id !== tenant.id) {
              return false;
            }
          }
        },
      },
    };

    return new HttpAuthxInterceptor<
      AppIdentityBill,
      AppIdentifiedBill<Principal>
    >({
      logger,
      authn: {
        principalFn,
        anonymousScopes: ANONYMOUS_SCOPES,
      },
      authz: {
        tree,
      },
      unauthorizedResponse: (req, res): ErrorDTO => ({
        id: req.header('x-correlation-id') || 'X-CORRELATION-ID-NOT-FOUND',
        statusCode: 401,
        message: 'Unauthorized.',
      }),
      forbiddenResponse: (req, res, scopes): ErrorDTO => ({
        id: req.header('x-correlation-id') || 'X-CORRELATION-ID-NOT-FOUND',
        statusCode: 403,
        message: `Forbidden access to scopes: ${scopes.join(', ')}`,
      }),
    });
  },
};
