import {
  AnonymousBill,
  IdentifiedBill,
  RightsTree,
} from '@eropple/nestjs-auth';
import { IdentifiedExpressRequest } from '@eropple/nestjs-auth/dist/helper-types';

import { User } from '../directory/db/User.entity';

export enum PrincipalType {
  USER = 'user',
  INTERNAL_SERVICE = 'internal_service',
}

// tslint:disable-next-line: no-empty-interface
export interface InternalService {}

export type Principal = User | InternalService;

export abstract class AppIdentifiedBill<
  TPrincipal extends Principal
> extends IdentifiedBill<TPrincipal, {}> {}

export class UserBill extends AppIdentifiedBill<User> {
  readonly type: PrincipalType.USER = PrincipalType.USER;
}

export class InternalServiceBill extends AppIdentifiedBill<InternalService> {
  readonly type: PrincipalType.INTERNAL_SERVICE =
    PrincipalType.INTERNAL_SERVICE;
}

export type AppAnyIdentifiedBill = UserBill | InternalServiceBill;
export type AppIdentityBill = AnonymousBill | AppAnyIdentifiedBill;

export type AppRightsTree = RightsTree<
  AppIdentityBill,
  IdentifiedExpressRequest<AppIdentityBill>
>;
