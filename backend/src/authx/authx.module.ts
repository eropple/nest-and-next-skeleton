import { LoggingModule } from '@eropple/nestjs-bunyan/dist';
import { Module } from '@nestjs/common';
import { FactoryProvider } from '@nestjs/common/interfaces';

import { ConfigService } from '../config/config.service';
import { DirectoryModule } from '../directory/directory.module';
import { SessionModule } from '../session/session.module';
import { authx } from './authx.provider';

const config: FactoryProvider = {
  provide: 'AuthxConfig',
  inject: [ConfigService],
  useFactory: (cfg: ConfigService) => cfg.authx,
};

@Module({
  imports: [LoggingModule, DirectoryModule, SessionModule],
  providers: [config, authx],
})
export class AuthxModule {}
