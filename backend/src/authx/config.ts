import { LogLevelString } from 'bunyan';

export interface AuthxConfig {
  readonly logLevel: LogLevelString;
}
