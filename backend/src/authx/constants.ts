export const ANONYMOUS_SCOPES: ReadonlyArray<string> = [
  'always',
  '**/tenant/public-info',
];

export const INTERNAL_SERVICE_HEADER_NAME = 'X-Internal-Auth'.toLowerCase();
export const USER_AUTH_HEADER_NAME = 'Authorization'.toLowerCase();
export const USER_COOKIE_NAME = 'AccessToken';
