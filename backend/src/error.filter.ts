import { Bunyan, Logger } from '@eropple/nestjs-bunyan/dist';
import * as OAS from '@eropple/nestjs-openapi3';
import { ArgumentsHost, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request as ExpressRequest, Response as ExpressResponse } from 'express';
import * as _ from 'lodash';
import { serializeError } from 'serialize-error';

@OAS.Model()
export class ErrorDTO {
  @OAS.Prop()
  id!: string;

  @OAS.Prop({ type: 'integer' })
  statusCode!: number;

  @OAS.Prop()
  message!: string;

  // intentionally not added to the OpenAPI model; will only be displayed in dev.
  stackTrace?: Array<string>;
}

@OAS.Model()
export class BadRequestDTO {
  @OAS.Prop()
  id!: string;

  @OAS.Prop({ type: 'integer' })
  statusCode!: number;

  @OAS.ArrayProp({ items: { type: 'string' }})
  failures!: Array<string>;
}

export class ErrorFilter implements ExceptionFilter {
  private readonly logger: Bunyan;

  constructor(
    logger: Bunyan,
    private readonly printResponseStackTraces: boolean,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
  }

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<ExpressResponse>();
    const request = ctx.getRequest<ExpressRequest>();

    let error: ErrorDTO | BadRequestDTO;

    const id = request.header('x-correlation-id') || 'X-CORRELATION-ID-NOT-FOUND';
    let statusCode = 500;

    if (exception instanceof Error) {
      if (exception instanceof HttpException) {
        statusCode = exception.getStatus();
      }

      if (statusCode !== 400) { // normal ErrorDTO
        error = { id, statusCode, message: exception.message };

        if (this.printResponseStackTraces) {
          error.stackTrace = exception.stack
            ? _.chain(exception.stack.split('\n')).drop(1).map(s => s.trim()).value()
            : [];
        }
      } else { // 400s are BadRequestDTOs, we specify this in `app-initialization.ts`
        error = { id, statusCode, failures: [exception.message] };
      }
    } else {
      error = { id, statusCode, message: 'An unexpected error occurred.' };
    }

    if (statusCode >= 500) {
      // ErrorDTO is guaranteed here.
      this.logger.error({ correlationId: id, err: serializeError(exception) }, (error as ErrorDTO).message);
    }

    response.status(statusCode).json(error);
  }
}
