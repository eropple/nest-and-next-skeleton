import { LogLevelString } from 'bunyan';
import { Duration } from 'luxon';
import { SessionConfig } from '../session/config';

import { envBoolean, envOrFallback } from '../utils/env';
import { ENV_PREFIX, TOKEN_PREFIX } from './_base';

const cfg: SessionConfig = {
  logLevel: envOrFallback(`${ENV_PREFIX}_SESSION_LOG_LEVEL`, 'info') as LogLevelString,
  tokenPrefix: TOKEN_PREFIX,
  loginTokenLifespan: Duration.fromObject({ months: 1 }),
  insecureTokenCookie: envBoolean(`${ENV_PREFIX}_SESSION_INSECURE_TOKEN_COOKIE`),
  noHttpOnlyCookie: envBoolean(`${ENV_PREFIX}_SESSION_NO_HTTP_ONLY_COOKIE`),
};

export default cfg;
