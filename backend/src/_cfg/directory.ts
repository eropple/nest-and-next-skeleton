import { LogLevelString } from 'bunyan';

import { DirectoryConfig } from '../directory/config';
import { envOrFail, envOrFallback } from '../utils/env';
import { ENV_PREFIX } from './_base';

const cfg: DirectoryConfig = {
  tenants: {},
  users: {
    logLevel: envOrFallback('USER_LOG_LEVEL', 'info') as LogLevelString,
    encryptionKeyB64: envOrFail(`${ENV_PREFIX}_USER_DATA_ENCRYPTION_KEY`),
  },
};

export default cfg;
