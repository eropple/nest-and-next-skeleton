// Most of these values should be set once - the first time you set up your app.
// After that, changing them (with the exception of stuff like `API_URL`) might
// cause demons to fly out your nose.

/**
 * Internally-used name for the web service. (This will _not_ be used for the CLI.)
 */
export const APPLICATION_NAME = 'Skeleton';

/**
 * When running as a CLI app, this will be the logger name.
 */
export const CLI_NAME = 'SkelCLI';

/**
 * Your application's env-var configs will be prefixed with this value. For
 * example, `SKEL` will result in env vars like `SKEL_LOG_LEVEL`.
 */
export const ENV_PREFIX = 'SKEL';

/**
 * The canonical endpoint at which this API can be reached. It need not be the
 * only endpoint, but it should always be a _valid_ one.
 */
export const API_URL = 'http://localhost:3000/';

/**
 * All login tokens will have this value appended to them. For login tokens,
 * with a relatively limited lifespan, but where this becomes really handy
 * is API tokens (which, if you think about it, aren't materially very
 * different from login tokens). You can then search GitHub, Pastebin, etc.
 * for leaked API tokens by searching for the string `SKEL__API_V1__`, then
 * invalidate them and let the users--okay, the _offenders_--know.
 */
export const TOKEN_PREFIX = 'SKEL';

/**
 * The default email address from which emails should be sent. Obviously
 * you'll want to make sure your email provider can actually send them;
 * in dev, Mailcatcher is very permissive, so this is a go-to-prod concern.
 */
export const MAIL_FROM = { name: APPLICATION_NAME, address: `my-app@example.com` };

/**
 * The default Reply-To for all emails. You might want to route replies to
 * automated addresses to a mailbox that a human can see, for example.
 */
export const MAIL_REPLY_TO = 'no-reply@example.com';
