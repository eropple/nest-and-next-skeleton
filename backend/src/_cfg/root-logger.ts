import Bunyan from 'bunyan';

import { envBoolean } from '../utils/env';
import { APPLICATION_NAME, CLI_NAME, ENV_PREFIX } from './_base';
import { IS_CLI } from './_computed';
import { ROOT_LOG_LEVEL } from './_log-levels';

const forcePrettyLogs = envBoolean(`${ENV_PREFIX}_FORCE_PRETTY_LOGS`) || IS_CLI;

const logger = Bunyan.createLogger({
  name: IS_CLI ? CLI_NAME : APPLICATION_NAME,
  level: ROOT_LOG_LEVEL,
  streams: forcePrettyLogs ? [
    {
      type: 'raw',
      stream: (() => {
        // tslint:disable-next-line: no-require-imports
        const PrettyStream = require('bunyan-prettystream-circularsafe');
        const pretty = new PrettyStream();
        pretty.pipe(process.stderr);

        return pretty;
      })(),
    },
  ] : undefined,
});

export default logger;
