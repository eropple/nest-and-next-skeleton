import { MailerOptions } from '@eropple/nestjs-rx-mailer';

import { envOrFail } from '../utils/env';
import { ENV_PREFIX, MAIL_FROM, MAIL_REPLY_TO } from './_base';

const ret: MailerOptions = {
  nodemailer: {
    transport: envOrFail(`${ENV_PREFIX}_MAILER_SMTP_TRANSPORT`),
    defaults: {
      from: MAIL_FROM,
      replyTo: MAIL_REPLY_TO,
    },
  },
};

export default ret;
