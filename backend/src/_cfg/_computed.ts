import { findUp } from '@eropple/find-up';
import { dirname } from 'path';

import { envBoolean, envOrFail } from '../utils/env';
import { ENV_PREFIX } from './_base';

// *** these are unlikely to need changing, but mi casa es su casa *** //
export const IS_CLI = envBoolean(`${ENV_PREFIX}_IS_CLI`);
export const PACKAGE_VERSION = envOrFail('npm_package_version');
export const PACKAGE_ROOT = dirname(findUp(__dirname, 'package.json')!);
