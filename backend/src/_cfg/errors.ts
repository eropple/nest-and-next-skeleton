import { envBoolean } from '../utils/env';

import { ENV_PREFIX } from './_base';

export const RESPONSE_STACK_TRACES = envBoolean(`${ENV_PREFIX}_RESPONSE_STACK_TRACES`);
