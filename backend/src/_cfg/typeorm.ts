import { TypeOrmLoggerAdapter } from '@eropple/typeorm-bunyan-logger';
import { LogLevelString } from 'bunyan';
import { ConnectionOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import path from 'path';

import { envBoolean, envIntOrFallback, envOrFail, envOrFallback } from '../utils/env';
import { ENV_PREFIX } from './_base';
import { DB_LOG_LEVEL } from './_log-levels';
import rootLogger from './root-logger';

const dbLogger = rootLogger.child({ component: 'TypeORM' });
dbLogger.level(DB_LOG_LEVEL);

const dbOptions: ConnectionOptions = {
  database: envOrFail(`${ENV_PREFIX}_DB_NAME`),
  host: envOrFail(`${ENV_PREFIX}_DB_HOST`),
  password: envOrFail(`${ENV_PREFIX}_DB_PASSWORD`),
  port: envIntOrFallback(`${ENV_PREFIX}_DB_PORT`, 5432),
  username: envOrFail(`${ENV_PREFIX}_DB_USER`),
  migrationsRun: envBoolean(`${ENV_PREFIX}_DB_RUN_MIGRATIONS`),
  dropSchema: envBoolean(`${ENV_PREFIX}_DB_DROP_SCHEMA`),

  //
  type: 'postgres',
  namingStrategy: new SnakeNamingStrategy(),
  entities: [path.resolve(__dirname, '..', '**', 'db', '*.entity.{ts,js}')],
  migrations: [path.resolve(__dirname, '..', 'db', 'migrations', '*.{ts,js}')],
  cli: {
    entitiesDir: `./src/db/entities`,
    migrationsDir: `./src/db/migrations`,
    subscribersDir: `./src/db/subscribers`,
  },
  logger: new TypeOrmLoggerAdapter(dbLogger),
  logging: true,
  synchronize: false,
};

export default dbOptions;
