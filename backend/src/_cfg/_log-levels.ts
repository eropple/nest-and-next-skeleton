import { LogLevelString } from 'bunyan';

import { envOrFallback } from '../utils/env';
import { ENV_PREFIX } from './_base';

export const ROOT_LOG_LEVEL = (envOrFallback(`${ENV_PREFIX}_LOG_LEVEL`, 'info')) as LogLevelString;

function level(suffix: string): LogLevelString {
  return (envOrFallback(`${ENV_PREFIX}_${suffix}`, ROOT_LOG_LEVEL)) as LogLevelString;
}

export const NEST_LOG_LEVEL    = level('NEST_LOG_LEVEL');
export const DB_LOG_LEVEL      = level('DB_LOG_LEVEL');
export const AUTHX_LOG_LEVEL   = level('AUTHX_LOG_LEVEL');
export const OPENAPI_LOG_LEVEL = level('OPENAPI_LOG_LEVEL');
