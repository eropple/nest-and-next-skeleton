import { AuthxConfig } from '../authx/config';
import { AUTHX_LOG_LEVEL } from './_log-levels';

const ret: AuthxConfig = {
  logLevel: AUTHX_LOG_LEVEL,
};

export default ret;
