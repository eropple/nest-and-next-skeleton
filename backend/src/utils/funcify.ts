export type Funcifiable<T> = (() => T) | T;

export function funcify<T>(v: Funcifiable<T>): T {
  if (typeof(v) === 'string') {
    return v;
  }

  // @ts-ignore
  return v();
}
