import * as Crypto from 'crypto';
import cryptoRandomString from 'crypto-random-string';
import {
  randomBytes,
  secretbox,
} from 'tweetnacl';
import {
  decodeBase64,
  decodeUTF8,
  encodeBase64,
  encodeUTF8,
} from 'tweetnacl-util';

function symmetricNonce() {
  return randomBytes(secretbox.nonceLength);
}

export function generateSymmetricKeyB64() {
  return encodeBase64(randomBytes(secretbox.keyLength));
}

export function sha256String(msg: string) {
  const hasher = Crypto.createHash('sha256');
  hasher.update(msg);
  return hasher.digest('hex');
}

/**
 * THIS IS A VERY BAD METHOD FOR YOU TO USE IN PRODUCTION. It
 * creates an "encryption key" that is just all zeroes so that
 * you always have an as-expected key in your dev environments,
 * in case you have to go poking around the database. Under no
 * circumstances is this appropriate for production. Don't do
 * it.
 */
export function UNSAFE_FOR_PROD__generateSymmetricDevKeyB64() {
  const arr = new Uint8Array(secretbox.keyLength);
  arr.fill(0);

  return encodeBase64(arr);
}

export function encryptSymmetricToB64(
  plaintextUTF8: string,
  keyB64: string,
): string {
  const keyU8 = decodeBase64(keyB64);

  const nonce = symmetricNonce();
  const messageU8 = decodeUTF8(plaintextUTF8);

  const box = secretbox(messageU8, nonce, keyU8);

  const encrypted = new Uint8Array(nonce.length + box.length);
  encrypted.set(nonce);
  encrypted.set(box, nonce.length);

  return encodeBase64(encrypted);
}

export function decryptSymmetricFromB64(
  encryptedWithNonceB64: string,
  keyB64: string,
): string {
  const keyUint8Array = decodeBase64(keyB64);
  const messageWithNonceAsUint8Array = decodeBase64(encryptedWithNonceB64);
  const nonce = messageWithNonceAsUint8Array.slice(0, secretbox.nonceLength);
  const message = messageWithNonceAsUint8Array.slice(
    secretbox.nonceLength,
    encryptedWithNonceB64.length,
  );

  const decrypted = secretbox.open(message, nonce, keyUint8Array);

  if (!decrypted) {
    throw new Error('Could not decrypt message (usually means a bad key).');
  }

  return encodeUTF8(decrypted);
}

export function randomAlphanumericString(length: number) {
  return cryptoRandomString({
    length,
    characters: '01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  });
}

export function timingSafeStringEquals(a: string, b: string) {
  return Crypto.timingSafeEqual(Buffer.from(a, 'utf8'), Buffer.from(b, 'utf8'));
}
