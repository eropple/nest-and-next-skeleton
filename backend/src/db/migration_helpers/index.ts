import { QueryRunner } from 'typeorm';

export async function attachUpdatedAt(q: QueryRunner, tableName: string) {
  return q.query(
    `
    CREATE TRIGGER ${tableName}_set_trigger
      BEFORE UPDATE ON ${tableName}
      FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp();
    `,
  );
}

export function detachUpdatedAt(q: QueryRunner, tableName: string) {
  return q.query(
    `
    DROP TRIGGER IF EXISTS ${tableName}_set_trigger ON ${tableName}
    `,
  );
}
