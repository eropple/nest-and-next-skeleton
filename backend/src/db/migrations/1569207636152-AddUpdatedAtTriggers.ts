import {MigrationInterface, QueryRunner} from 'typeorm';
import { attachUpdatedAt, detachUpdatedAt } from '../migration_helpers';

export class AddUpdatedAtTriggers1569207636152 implements MigrationInterface {
    private readonly tableNames = ['tenants', 'tenant_oidc_idps', 'users'];

    public async up(queryRunner: QueryRunner): Promise<any> {
      for (const tableName of this.tableNames) {
        await attachUpdatedAt(queryRunner, tableName);
      }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      for (const tableName of this.tableNames) {
        await detachUpdatedAt(queryRunner, tableName);
      }
    }

}
