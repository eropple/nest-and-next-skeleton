import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdatedAtProc0000000000001 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `
        CREATE OR REPLACE FUNCTION trigger_set_timestamp()
        RETURNS TRIGGER AS $$
        BEGIN
          NEW.updated_at = NOW();
          RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
        `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
