import {MigrationInterface, QueryRunner} from 'typeorm';

export class InitialTables1569207589502 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "tenant_oidc_idps" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE, "request_uri" text NOT NULL, "request_scopes" text NOT NULL DEFAULT '', "client_id" text NOT NULL, "encrypted_client_secret" text NOT NULL, "create_user_if_nonexistent" boolean NOT NULL, "allow_local_passwords_when_used" boolean NOT NULL, "group_claim_name" text, "update_profile_from_provider" boolean NOT NULL, "tenant_id" uuid, CONSTRAINT "PK_73c6e956d7bb1956dd751aee0a0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_d140b962f3fc089f008ca96301" ON "tenant_oidc_idps" ("name", "tenant_id") `);
        await queryRunner.query(`CREATE TABLE "tenants" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, "display_name" text NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE, CONSTRAINT "UQ_32731f181236a46182a38c992a8" UNIQUE ("name"), CONSTRAINT "PK_53be67a04681c66b87ee27c9321" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "user_passwords_scheme_enum" AS ENUM('argon2')`);
        await queryRunner.query(`CREATE TABLE "user_passwords" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "scheme" "user_passwords_scheme_enum" NOT NULL, "hash" text NOT NULL, "user_id" uuid, CONSTRAINT "REL_69bf155ad044d776976470eb03" UNIQUE ("user_id"), CONSTRAINT "PK_4244bafe3ae2988e7bb7af61268" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_totps" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "totp_secret_encrypted" text NOT NULL, "validated" boolean NOT NULL DEFAULT false, "user_id" uuid, CONSTRAINT "REL_d8b57340c19ab0d207d4fd8c99" UNIQUE ("user_id"), CONSTRAINT "PK_7eb7f99a78fba7ed2094702a6e5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, "display_name" text NOT NULL, "email" text NOT NULL, "active" boolean NOT NULL DEFAULT true, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE, "tenant_id" uuid, CONSTRAINT "UQ_51b8b26ac168fbe7d6f5653e6cf" UNIQUE ("name"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_e9f4c2efab52114c4e99e28efb" ON "users" ("tenant_id", "email") `);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_c1ff42886941a20e4854e5b2a1" ON "users" ("tenant_id", "name") `);
        await queryRunner.query(`CREATE TYPE "access_tokens_type_enum" AS ENUM('login')`);
        await queryRunner.query(`CREATE TABLE "access_tokens" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "type" "access_tokens_type_enum" NOT NULL, "salt" text NOT NULL, "hash" text NOT NULL, "grants" text array NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "expires_at" TIMESTAMP WITH TIME ZONE NOT NULL, "revoked_at" TIMESTAMP WITH TIME ZONE NULL, "user_id" uuid, CONSTRAINT "UQ_a6259f10622b13216ac8508d0d9" UNIQUE ("salt"), CONSTRAINT "PK_65140f59763ff994a0252488166" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "tenant_oidc_idps" ADD CONSTRAINT "FK_bc0bc72cc578ee9af4aacf337a9" FOREIGN KEY ("tenant_id") REFERENCES "tenants"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_passwords" ADD CONSTRAINT "FK_69bf155ad044d776976470eb032" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE RESTRICT`);
        await queryRunner.query(`ALTER TABLE "user_totps" ADD CONSTRAINT "FK_d8b57340c19ab0d207d4fd8c999" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE RESTRICT`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_109638590074998bb72a2f2cf08" FOREIGN KEY ("tenant_id") REFERENCES "tenants"("id") ON DELETE RESTRICT ON UPDATE RESTRICT`);
        await queryRunner.query(`ALTER TABLE "access_tokens" ADD CONSTRAINT "FK_09ee750a035b06e0c7f0704687e" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "access_tokens" DROP CONSTRAINT "FK_09ee750a035b06e0c7f0704687e"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_109638590074998bb72a2f2cf08"`);
        await queryRunner.query(`ALTER TABLE "user_totps" DROP CONSTRAINT "FK_d8b57340c19ab0d207d4fd8c999"`);
        await queryRunner.query(`ALTER TABLE "user_passwords" DROP CONSTRAINT "FK_69bf155ad044d776976470eb032"`);
        await queryRunner.query(`ALTER TABLE "tenant_oidc_idps" DROP CONSTRAINT "FK_bc0bc72cc578ee9af4aacf337a9"`);
        await queryRunner.query(`DROP TABLE "access_tokens"`);
        await queryRunner.query(`DROP TYPE "access_tokens_type_enum"`);
        await queryRunner.query(`DROP INDEX "IDX_c1ff42886941a20e4854e5b2a1"`);
        await queryRunner.query(`DROP INDEX "IDX_e9f4c2efab52114c4e99e28efb"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TABLE "user_totps"`);
        await queryRunner.query(`DROP TABLE "user_passwords"`);
        await queryRunner.query(`DROP TYPE "user_passwords_scheme_enum"`);
        await queryRunner.query(`DROP TABLE "tenants"`);
        await queryRunner.query(`DROP INDEX "IDX_d140b962f3fc089f008ca96301"`);
        await queryRunner.query(`DROP TABLE "tenant_oidc_idps"`);
    }

}
