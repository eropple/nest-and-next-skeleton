import { buildApp, configureApp } from './app-initialization';

async function bootstrap() {
  const app = await buildApp();
  await configureApp(app);

  await app.listen(3000);
}

// tslint:disable-next-line: no-floating-promises
bootstrap();
