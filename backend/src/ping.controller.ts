import {
  AuthnDisallowed,
  AuthnOptional,
  AuthzScope,
  Identity,
} from '@eropple/nestjs-auth';
import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import * as OAS from '@eropple/nestjs-openapi3';
import { Mailer, RxMailer } from '@eropple/nestjs-rx-mailer';
import { Controller, Get, Header, HttpCode, Request, Scope } from '@nestjs/common';

import { AppAnyIdentifiedBill, AppIdentityBill } from './authx/identity-types';
import { Tenant } from './directory/db/Tenant.entity';
import { ReqTenant } from './directory/tenant.decorator';

@OAS.Model()
export class PingResponse {
  @OAS.Prop()
  ping: string;

  @OAS.Prop()
  authed: boolean;

  constructor(authed: boolean) {
    this.ping = 'Ping!';
    this.authed = authed;
  }
}

@OAS.Tags('meta')
@Controller({ scope: Scope.REQUEST })
export class PingController {
  private readonly logger: Bunyan;

  constructor(
    @Logger() logger: Bunyan,
    @RxMailer() private readonly mailer: Mailer,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
  }

  @AuthnOptional()
  @AuthzScope('always')
  @OAS.Get('ping', {
    summary: 'Ping',
    responses: {
      200: {
        description: 'Returns a ping.',
        content: PingResponse,
      },
    },
  })
  ping(@Identity() identity: AppIdentityBill): PingResponse {
    this.logger.info('Ping!');
    return new PingResponse(identity.isIdentified);
  }

  @AuthnOptional()
  @AuthzScope('always')
  @OAS.Get('ping/any', {
    summary: 'Ping (no identity check)',
    responses: {
      200: {
        description: 'Returns a ping.',
        content: PingResponse,
      },
    },
  })
  pingAny(): PingResponse {
    this.logger.info('Ping!');
    return new PingResponse(false);
  }

  @AuthzScope('always')
  @OAS.Get('ping/authed', {
    summary: 'Ping (Authed Only)',
    responses: {
      200: {
        description: 'Returns a ping.',
        content: PingResponse,
      },
    },
  })
  @OAS.SecurityScheme('token')
  pingAuthed(
    @Identity() identity: AppAnyIdentifiedBill,
  ): PingResponse {
    this.logger.info('Ping!');
    return new PingResponse(identity.isIdentified);
  }

  @AuthnDisallowed()
  @AuthzScope('always')
  @OAS.Get('ping/unauthed', {
    summary: 'Ping (Unauthed Only)',
    responses: {
      200: {
        description: 'Returns a ping.',
        content: PingResponse,
      },
    },
  })
  pingUnauthed(@Identity() identity: AppAnyIdentifiedBill): PingResponse {
    this.logger.info('Ping!');
    return new PingResponse(identity.isIdentified);
  }
}
