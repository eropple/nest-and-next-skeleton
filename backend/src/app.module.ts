import { LoggingModule } from '@eropple/nestjs-bunyan';
import { RxMailerModule } from '@eropple/nestjs-rx-mailer';
import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthxModule } from './authx/authx.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { DirectoryModule } from './directory/directory.module';
import { PingController } from './ping.controller';
import { RootController } from './root.controller';
import { SessionModule } from './session/session.module';

import { IS_CLI } from './_cfg/_computed';
import rootLogger from './_cfg/root-logger';
import { HealthController } from './health.controller';

const imports = [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => config.typeorm,
    }),
    RxMailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => config.mailer,
    }),
    DirectoryModule,
    SessionModule,
];

const modeImports =
  IS_CLI
    ? [
      LoggingModule.forRoot(rootLogger, { staticLogger: true }),
    ]
    : [
      LoggingModule.forRoot(rootLogger, { staticLogger: false, ipSalt: 'some-salt' }),
      AuthxModule
    ];

@Module({
  imports: [
    ...modeImports,
    ...imports,
    TerminusModule,
  ],
  controllers: [
    PingController,
    RootController,
    HealthController,
  ],
  providers: [],
})
export class AppModule {}
