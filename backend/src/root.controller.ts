import {
  AuthnOptional,
  AuthzScope,
} from '@eropple/nestjs-auth';
import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import * as OAS from '@eropple/nestjs-openapi3';
import { Mailer, RxMailer } from '@eropple/nestjs-rx-mailer';
import { Controller, Get, Header, HttpCode, HttpException, Scope } from '@nestjs/common';
import React from 'react'; // could allow you to use JSX if you wanted

import { OkResponse } from './domain';
import { TestEmail } from './test-email.email';

@OAS.Model()
export class EchoRequest {
  @OAS.Prop({ default: 'hello' }, { required: false })
  readonly a!: string;
  @OAS.Prop()
  readonly b!: number;
}

@OAS.Tags('meta')
@Controller({ scope: Scope.REQUEST })
export class RootController {
  private readonly logger: Bunyan;

  constructor(
    @Logger() logger: Bunyan,
    @RxMailer() private readonly mailer: Mailer,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
  }

  @AuthnOptional()
  @AuthzScope('always')
  @Get('robots.txt')
  @Header('content-type', 'text/plain')
  @OAS.Get({
    response: {
      multiContent: {
        'text/plain': { type: 'string' },
      },
    },
  })
  robotsTxt() {
    return (
      `User-agent: *
      Disallow: /`
    );
  }

  @AuthnOptional()
  @AuthzScope('always')
  @HttpCode(200)
  @OAS.Post('echo/:foo', {
    responses: {
      200: {
        multiContent: {
          'text/plain': { type: 'string' },
        },
      },
    },
  })
  @OAS.Deprecated()
  @Header('content-type', 'text/plain')
  echo(@OAS.Body() body: EchoRequest, @OAS.Path('foo') foo: string, @OAS.Query('bar') bar: number, @OAS.Header('baz') baz: string): string {
    return `${foo} (${typeof foo}) -- ${bar} (${typeof bar}) -- ${baz} (${typeof baz}) -- ${JSON.stringify(body)}`;
  }

  @AuthnOptional()
  @AuthzScope('always')
  @OAS.Get('throw-error')
  throwError() {
    throw new Error('This is an unhandled exception.');
  }

  @AuthnOptional()
  @AuthzScope('always')
  @OAS.Get('email-test', {
    responses: {
      200: { content: OkResponse },
    },
  })
  async testEmail(): Promise<OkResponse> {
    // I go back and forth on whether to use `React.createElement` here or to rename
    // the controller to `ping.controller.tsx` and use JSX. It doesn't really matter;
    // pick your favorite option!
    await this.mailer.sendEmail({
      subject: 'A Good Test Email',
      to: 'somebody@example.com',
      html: React.createElement(TestEmail, {
        title: 'A Good Test Email',
        text: 'Pretty good email, huh?',
      }),
    });
    return new OkResponse();
  }
}
