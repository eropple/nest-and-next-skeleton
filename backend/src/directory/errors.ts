import { ConflictException, HttpException, HttpStatus, InternalServerErrorException, NotFoundException } from '@nestjs/common';

export class TenantAlreadyExistsException extends ConflictException {
  constructor(msg: string | {} = 'Tenant already exists.') {
    super(msg);
  }
}

export class TenantNotFoundException extends NotFoundException {
  constructor(msg: string | {} = 'Tenant not found.') {
    super(msg);
  }
}

export class TenantUserMismatchException extends InternalServerErrorException {
  constructor(msg: string | {} = 'Tenant/user ID mismatch.') {
    super(msg);
  }
}

export class UserNotFoundException extends NotFoundException {
  constructor(msg: string | {} = 'User not found.') {
    super(msg);
  }
}
