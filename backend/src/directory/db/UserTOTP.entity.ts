import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { User } from './User.entity';

@Entity({ name: 'user_totps' })
export class UserTOTP {
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @OneToOne(t => User, o => o.password, { onDelete: 'RESTRICT', onUpdate: 'RESTRICT' })
  @JoinColumn()
  readonly user!: User;

  @CreateDateColumn({ nullable: false })
  readonly createdAt!: Date;

  @Column('text', { nullable: false })
  readonly totpSecretEncrypted!: string;

  @Column({ default: false })
  validated!: boolean;
}
