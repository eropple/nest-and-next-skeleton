import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { User } from './User.entity';

export enum UserPasswordScheme {
  ARGON2 = 'argon2',
}
export const UserPasswordSchemeValues = [UserPasswordScheme.ARGON2];

@Entity({ name: 'user_passwords' })
export class UserPassword {
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @OneToOne(t => User, o => o.password, { onDelete: 'RESTRICT', onUpdate: 'RESTRICT' })
  @JoinColumn()
  readonly user!: User;

  @CreateDateColumn({ nullable: false })
  readonly createdAt!: Date;

  @Column({ type: 'enum', enum: UserPasswordSchemeValues })
  readonly scheme!: UserPasswordScheme;

  @Column('text', { nullable: false })
  readonly hash!: string;
}
