import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Tenant } from './Tenant.entity';

@Entity({ name: 'tenant_oidc_idps' })
@Index(['name', 'tenant'], { unique: true })
export class TenantOIDCIdP {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @ManyToOne(t => Tenant, o => o.oidcIdPs)
  @JoinColumn()
  readonly tenant!: Tenant;

  @Column('text', { nullable: false })
  readonly name!: string;

  @CreateDateColumn({ nullable: false })
  readonly createdAt!: Date;

  @Column('timestamptz', { nullable: true })
  readonly updatedAt!: Date | null;

  @Column('text', { nullable: false })
  requestUri!: string;

  @Column('text', { nullable: false, default: '' })
  requestScopes!: string;

  @Column('text', { nullable: false })
  clientId!: string;

  @Column('text', { nullable: false })
  encryptedClientSecret!: string;

  @Column('boolean', { nullable: false })
  createUserIfNonexistent!: boolean;

  @Column('boolean', { nullable: false })
  allowLocalPasswordsWhenUsed!: boolean;

  @Column('text', { nullable: true })
  groupClaimName!: string | null;

  @Column('boolean', { nullable: false })
  updateProfileFromProvider!: boolean;
}
