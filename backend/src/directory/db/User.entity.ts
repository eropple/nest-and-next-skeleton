import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { UserPrivate } from '../domain/UserPrivate.domain';
import { UserPublic } from '../domain/UserPublic.domain';
import { Tenant } from './Tenant.entity';
import { UserPassword } from './UserPassword.entity';
import { UserTOTP } from './UserTOTP.entity';

@Entity({ name: 'users' })
@Index(['tenant', 'name'], { unique: true })
@Index(['tenant', 'email'], { unique: true })
export class User {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @ManyToOne(t => Tenant, { eager: true, onDelete: 'RESTRICT', onUpdate: 'RESTRICT' })
  @JoinColumn()
  readonly tenant!: Tenant;

  @Column('text', { unique: true, nullable: false })
  readonly name!: string;

  @Column('text', { nullable: false })
  displayName!: string;

  @Column('text', { nullable: false })
  email!: string;

  @Column('boolean', { nullable: false, default: true })
  active!: boolean;

  @CreateDateColumn({ nullable: false })
  readonly createdAt!: Date;

  @Column('timestamptz', { nullable: true })
  readonly updatedAt!: Date | null;

  @OneToOne(t => UserPassword, o => o.user, { eager: true })
  password!: UserPassword | null;

  @OneToOne(t => UserTOTP, o => o.user, { eager: true })
  totp!: UserTOTP | null;
}
