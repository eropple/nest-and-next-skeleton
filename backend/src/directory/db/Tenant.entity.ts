import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { TenantOIDCIdP } from './TenantOIDCIdP.entity';

export enum TenantState {
  AWAITING_VERIFICATION = 'awaiting_verification',
  VERIFIED = 'verified',
  CLOSED = 'closed',
}

@Entity({ name: 'tenants' })
export class Tenant {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  /**
   * This is the internal referent for your tenant. I like to use hostnames for
   * the visible application. For example, if I have tenants on
   * `TENANTNAME.example.com`, I'll have the `name` be something like
   * `bobcorp.example.com`. If you follow this approach I do _not_ recommend
   * that you truncate it to `bobcorp`; future you might want to be able to deal
   * with, say, `bigbobcorp.com` and white-label it to their own domain.
   *
   * Even if I have only one tenant, i.e. it's a SaaS without white-labeling, I
   * like to name the tenant in a way that reflects my application environments.
   * I find it a lot easier to work if, for service `example.com`, the
   * production tenant is named `example.com` and the dev tenant, even though
   * it's in a different environment and uses a different database, something
   * like `dev.example.com`. I can at a glance understand what I'm working with.
   */
  @Column('text', { unique: true, nullable: false })
  readonly name!: string;

  @Column('text', { nullable: false })
  displayName!: string;

  @CreateDateColumn({ nullable: false })
  readonly createdAt!: Date;

  @Column('timestamptz', { nullable: true })
  readonly updatedAt!: Date | null;

  @OneToMany(t => TenantOIDCIdP, o => o.tenant)
  oidcIdPs?: Array<TenantOIDCIdP>;
}
