import { Bunyan, InjectorKeys } from '@eropple/nestjs-bunyan';
import { Module } from '@nestjs/common';
import { FactoryProvider } from '@nestjs/common/interfaces';
import { getRepositoryToken } from '@nestjs/typeorm';
import _ from 'lodash';
import { Repository } from 'typeorm';

import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '../config/config.service';
import { DirectoryConfig, TenantConfig, UserConfig } from './config';
import { Tenant } from './db/Tenant.entity';
import { User } from './db/User.entity';
import { UserPassword } from './db/UserPassword.entity';
import { UserTOTP } from './db/UserTOTP.entity';
import { TenantController } from './tenant.controller';
import { TenantService } from './tenant.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

const config: FactoryProvider = {
  provide: 'DirectoryConfig',
  inject: [ConfigService],
  useFactory: (cfgService: ConfigService) => cfgService.directory,
};

const userConfig: FactoryProvider = {
  provide: 'UserConfig',
  inject: ['DirectoryConfig'],
  useFactory: (cfg: DirectoryConfig) => cfg.users,
};

const tenantConfig: FactoryProvider = {
  provide: 'TenantConfig',
  inject: ['DirectoryConfig'],
  useFactory: (cfg: DirectoryConfig) => cfg.tenants,
};

/**
 * This exists because there are some use cases (mostly ones in
 * testing, but it's conceivable that they exist elsewhere) where
 * we'll need access to a TenantService outside of a request
 * scope.
 */
const singletonTenantService: FactoryProvider = {
  provide: TenantService.SINGLETON_TOKEN,
  inject: [
    InjectorKeys.ROOT_LOGGER,
    'TenantConfig',
    getRepositoryToken(Tenant),
  ],
  useFactory: (
    logger: Bunyan,
    cfg: TenantConfig,
    tenants: Repository<Tenant>,
  ) => new TenantService(logger, cfg, tenants),
};

/**
 * This exists because there are some use cases (mostly ones in
 * testing, but it's conceivable that they exist elsewhere) where
 * we'll need access to a TenantService outside of a request
 * scope.
 */
const singletonUserService: FactoryProvider = {
  provide: UserService.SINGLETON_TOKEN,
  inject: [
    InjectorKeys.ROOT_LOGGER,
    'UserConfig',
    getRepositoryToken(User),
    getRepositoryToken(UserPassword),
    getRepositoryToken(UserTOTP),
    'singleton:TenantService',
  ],
  useFactory: (
    logger: Bunyan,
    cfg: UserConfig,
    users: Repository<User>,
    userPasswords: Repository<UserPassword>,
    userTotps: Repository<UserTOTP>,
    tenants: TenantService,
  ) => new UserService(logger, cfg, users, userPasswords, userTotps, tenants),
};

@Module({
  imports: [TypeOrmModule.forFeature([Tenant, User, UserPassword, UserTOTP])],
  providers: [
    config,
    userConfig,
    tenantConfig,
    TenantService,
    singletonTenantService,
    UserService,
    singletonUserService,
  ],
  controllers: [TenantController, UserController],
  exports: [
    TenantService,
    singletonTenantService,
    UserService,
    singletonUserService,
  ],
})
export class DirectoryModule {}
