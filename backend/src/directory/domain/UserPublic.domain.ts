import * as OAS from '@eropple/nestjs-openapi3';

import { User } from '../db/User.entity';

@OAS.Model()
export class UserPublic {
  @OAS.Prop()
  readonly id!: string;

  @OAS.Prop()
  readonly tenantId!: string;

  @OAS.Prop()
  readonly name!: string;

  @OAS.Prop()
  readonly displayName!: string;

  constructor(id: string, tenantId: string, name: string, displayName: string) {
    this.id = id;
    this.tenantId = tenantId;
    this.name = name;
    this.displayName = displayName;
  }

  static fromUser(user: User): UserPublic {
    return new UserPublic(user.id, user.tenant.id, user.name, user.displayName);
  }
}
