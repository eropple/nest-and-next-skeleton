import * as OAS from '@eropple/nestjs-openapi3';

import { User } from '../db/User.entity';
import { UserPublic } from './UserPublic.domain';

export class UserPrivate extends UserPublic {
  @OAS.Prop()
  readonly email!: string;

  @OAS.Prop()
  readonly active!: boolean;

  @OAS.Prop()
  readonly hasPassword!: boolean;

  @OAS.Prop()
  readonly has2FA!: boolean;

  constructor(
    id: string,
    tenantId: string,
    name: string,
    displayName: string,
    email: string,
    active: boolean,
    hasPassword: boolean,
    has2FA: boolean,
  ) {
    super(id, tenantId, name, displayName);

    this.email = email;
    this.active = active;
    this.hasPassword = hasPassword;
    this.has2FA = has2FA;
  }

  static fromUser(user: User): UserPrivate {
    return new UserPrivate(
      user.id,
      user.tenant.id,
      user.name,
      user.displayName,
      user.email,
      user.active,
      !!user.password,
      !!(user.totp && user.totp.validated),
    );
  }
}
