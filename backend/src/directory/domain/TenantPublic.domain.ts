import * as OAS from '@eropple/nestjs-openapi3';

import { Tenant } from '../db/Tenant.entity';

@OAS.Model()
export class TenantPublic {
  @OAS.Prop()
  readonly id: string;

  @OAS.Prop()
  readonly name: string;

  @OAS.Prop()
  readonly displayName: string;

  constructor(id: string, name: string, displayName: string) {
    this.id = id;
    this.name = name;
    this.displayName = displayName;
  }

  static fromTenant(tenant: Tenant): TenantPublic {
    return new TenantPublic(tenant.id, tenant.name, tenant.displayName);
  }
}
