import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import { BadRequestException, Inject, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as argon2 from 'argon2';
import cryptoRandomString from 'crypto-random-string';
import * as Speakeasy from 'speakeasy';
import { Repository } from 'typeorm';

import { APPLICATION_NAME } from '../_cfg/_base';
import { decryptSymmetricFromB64, encryptSymmetricToB64 } from '../utils/crypt';
import { UserConfig } from './config';
import { Tenant } from './db/Tenant.entity';
import { User } from './db/User.entity';
import { UserPassword, UserPasswordScheme } from './db/UserPassword.entity';
import { UserTOTP } from './db/UserTOTP.entity';
import { TenantUserMismatchException, UserNotFoundException } from './errors';
import { TenantService } from './tenant.service';

@Injectable()
export class UserService {
  static SINGLETON_TOKEN = 'singleton:UserService';

  private readonly logger: Bunyan;

  constructor(
    @Logger() logger: Bunyan,
    @Inject('UserConfig') private readonly config: UserConfig,
    @InjectRepository(User) private readonly users: Repository<User>,
    @InjectRepository(UserPassword) private readonly userPasswords: Repository<UserPassword>,
    @InjectRepository(UserTOTP) private readonly userTotps: Repository<UserTOTP>,
    private readonly tenants: TenantService,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
    this.logger.level(this.config.logLevel);
  }

  async forTenant(tenant: string | Tenant): Promise<UserServiceForTenant> {
    const t = typeof(tenant) === 'string' ? await this.tenants.getTenantByNameOrThrow(tenant) : tenant;

    return new UserServiceForTenant(
      t,
      this.logger,
      this.config,
      this.users,
      this.userPasswords,
      this.userTotps,
    );
  }

  async forUserInTenant(tenant: string | Tenant, identity: string | User): Promise<UserServiceForUserInTenant> {
    const serviceForTenant = await this.forTenant(tenant);
    return serviceForTenant.forUser(identity);
  }
}

export class UserServiceForTenant {
  private readonly logger: Bunyan;

  constructor(
    readonly tenant: Tenant,
    logger: Bunyan,
    private readonly config: UserConfig,
    private readonly users: Repository<User>,
    private readonly userPasswords: Repository<UserPassword>,
    private readonly userTotps: Repository<UserTOTP>,
  ) {
    this.logger = logger.child({ tenant: tenant.name });
  }

  async forUser(identity: string | User): Promise<UserServiceForUserInTenant> {
    const user =
      typeof(identity) === 'string'
        ? await this.users.findOne({ where: [
          { name: identity },
          { email: identity },
        ]})
        : identity;

    if (!user) {
      throw new UserNotFoundException();
    }

    if (user.tenant.id !== this.tenant.id) {
      // throw new TenantUserMismatchException();
    }

    return new UserServiceForUserInTenant(
      this.tenant,
      user,
      this.logger,
      this.config,
      this.users,
      this.userPasswords,
      this.userTotps,
    );
  }

  async createUser(
    username: string,
    email: string,
    passwordPlaintext?: string,
  ): Promise<[User, string | null]> {
    username = username.toLocaleLowerCase();
    const logger = this.logger.child({ user: username });

    logger.debug('Creating new user.');
    let user = await this.users.save({
      name: username,
      displayName: username,
      email,
      tenant: this.tenant,
    });

    logger.info('New user created.');

    if (passwordPlaintext) {
      const serviceForUser = await this.forUser(username);
      [user] = await serviceForUser.setPassword(passwordPlaintext);
    }

    return [user, passwordPlaintext || null];
  }
}

export class UserServiceForUserInTenant {
  private readonly logger: Bunyan;

  constructor(
    readonly tenant: Tenant,
    public user: User,
    logger: Bunyan,
    private readonly config: UserConfig,
    private readonly users: Repository<User>,
    private readonly userPasswords: Repository<UserPassword>,
    private readonly userTotps: Repository<UserTOTP>,
  ) {
    this.logger = logger.child({ user: user.name });
  }

  private async refresh(): Promise<User> {
    const u = await this.users.findOne({ id: this.user.id });
    if (!u) {
      this.logger.error('User failed to refresh; has state changed?');
      throw new InternalServerErrorException();
    }

    this.user = u;
    return u;
  }

  async removePassword(): Promise<User> {
    if (this.user.password) {
      this.logger.info('Removing old password for user.');
      await this.userPasswords.delete({ id: this.user.password.id });

      return this.refresh();
    }

    return this.user;
  }

  async setPassword(plaintext: string): Promise<[User, string]> {
    this.logger.info('Setting new password for user.');
    const scheme: UserPasswordScheme = UserPasswordScheme.ARGON2;
    const hash: string = await argon2.hash(plaintext, { raw: false });

    await this.removePassword();

    await this.userPasswords.save({ hash, scheme, user: this.user });
    return [await this.refresh(), plaintext];
  }

  async resetPassword(): Promise<[User, string]> {
    this.logger.info('Resetting user password.');
    const newPassword = cryptoRandomString({ length: 12, type: 'hex' });
    return this.setPassword(newPassword);
  }

  async clearTOTP(): Promise<User> {
    if (this.user.totp) {
      this.logger.info('Clearing TOTP for user.');
      await this.userTotps.delete({ id: this.user.totp.id });

      return this.refresh();
    }

    return this.user;
  }

  async setTOTP(): Promise<{ user: User, secret: string }> {
    await this.clearTOTP();

    this.logger.info('Creating TOTP secret for user. Not yet validated.');
    const issuer = `${APPLICATION_NAME} (${this.tenant.displayName})`;
    const secret = Speakeasy.generateSecret({ issuer });
    const retSecret = secret.base32;

    await this.userTotps.save({
      totpSecretEncrypted: encryptSymmetricToB64(retSecret, this.config.encryptionKeyB64),
      user: this.user,
    });
    const ret = {
      user: await this.refresh(),
      secret: retSecret,
    };
    return ret;
  }

  async validateTOTP(token: string) {
    const user = this.user;

    if (!user.totp) {
      throw new BadRequestException('Cannot validate TOTP if no TOTP exists on user.');
    }

    const valid = this.checkTOTP(token);

    if (!valid) {
      throw new BadRequestException('TOTP code does not match expected. Cannot validate.');
    }

    this.logger.info('TOTP validated.');
    const userTotp = user.totp;
    userTotp.validated = true;

    await this.userTotps.save(userTotp);
    return this.refresh();
  }

  checkTOTP(token: string): boolean {
    const user = this.user;

    if (!user.totp) {
      return false;
    }

    const secret = decryptSymmetricFromB64(user.totp.totpSecretEncrypted, this.config.encryptionKeyB64);
    const result = Speakeasy.totp.verifyDelta({
      secret,
      token,
      encoding: 'base32',
    });

    if (!result) {
      return false;
    }

    return Math.abs(result.delta) < 3;
  }
}
