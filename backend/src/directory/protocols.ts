import * as OAS from '@eropple/nestjs-openapi3';

@OAS.Model()
export class ChangePasswordResponse {
  @OAS.Prop()
  plaintext: string;

  constructor(plaintext: string) {
    this.plaintext = plaintext;
  }
}
