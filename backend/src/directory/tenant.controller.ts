import { AuthnOptional, AuthzScope } from '@eropple/nestjs-auth';
import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import * as OAS from '@eropple/nestjs-openapi3';
import { Controller, Get, Scope } from '@nestjs/common';

import { Tenant } from './db/Tenant.entity';
import { TenantPublic } from './domain/TenantPublic.domain';
import { TenantNotFoundException } from './errors';
import { ReqTenant } from './tenant.decorator';
import { TenantService } from './tenant.service';

@Controller({ path: ':tenantName/detail', scope: Scope.REQUEST })
@OAS.Tags('directory')
@OAS.Parameter({
  name: 'tenantName',
  in: 'path',
  required: true,
  schema: { type: 'string' },
})
export class TenantController {
  constructor(@Logger() logger: Bunyan, private readonly tenants: TenantService) {}

  @Get()
  @AuthnOptional()
  @AuthzScope('always')
  @OAS.Get({
    summary: 'Get Public Tenant Info',
    responses: {
      200: { content: TenantPublic },
    },
  })
  async getPublicTenantInfo(@ReqTenant() tenant: Tenant): Promise<Tenant> {
    if (!tenant) {
      throw new TenantNotFoundException();
    }

    return tenant;
  }
}
