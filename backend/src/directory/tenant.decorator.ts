import { IdentifiedExpressRequest } from '@eropple/nestjs-auth/dist';
import { createParamDecorator } from '@nestjs/common';

export const ReqTenant = createParamDecorator(
  (data, req: IdentifiedExpressRequest) => {
    return req.locals.tenant;
  },
);
