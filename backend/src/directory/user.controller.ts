import * as OAS from '@eropple/nestjs-openapi3';
import { Controller } from '@nestjs/common';

@Controller({ path: ':tenantName/users' })
@OAS.Tags('directory')
@OAS.Parameter({
  name: 'tenantName',
  in: 'path',
  required: true,
  schema: { type: 'string' },
})
export class UserController {}
