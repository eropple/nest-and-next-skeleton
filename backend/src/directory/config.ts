import { LogLevelString } from 'bunyan';

export interface UserConfig {
  logLevel: LogLevelString;
  encryptionKeyB64: string;
}

export interface TenantConfig {

}

export interface DirectoryConfig {
  tenants: TenantConfig;
  users: UserConfig;
}
