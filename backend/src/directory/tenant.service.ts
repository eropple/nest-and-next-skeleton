import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';

import { AppRightsTree } from '../authx/identity-types';
import { TenantConfig } from './config';
import { Tenant } from './db/Tenant.entity';
import { TenantAlreadyExistsException, TenantNotFoundException } from './errors';

export interface CreateTenantArgs {
  name: string;
  displayName?: string;
}

@Injectable()
export class TenantService {
  static SINGLETON_TOKEN = 'singleton:TenantService';

  readonly tree: AppRightsTree = {
    wildcard: {
      children: {
        'public-info': {
          right: () => true,
        },
      },
    },
  };

  private readonly logger: Bunyan;

  constructor(
    @Logger() logger: Bunyan,
    @Inject('TenantConfig') private readonly config: TenantConfig,
    @InjectRepository(Tenant) private readonly tenants: Repository<Tenant>,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
  }

  async createTenant({
    name,
    displayName,
  }: CreateTenantArgs): Promise<Tenant> {
    if ((await this.getTenantByName(name))) {
      throw new TenantAlreadyExistsException(name);
    }

    const newTenant = await this.tenants.save({
      name,
      displayName: displayName || name,
    });

    return newTenant;
  }

  async getTenantByName(tenantName: string): Promise<Tenant | null> {
    // This is a "shouldn't happen" case, but because of the way NestJS
    // does some unsafe type stuff it has happened when the authz tree
    // tries to do stuff. Since without this the below `findOne` will
    // be queried with an empty array of conditions, it will return
    // the first condition and bomb out hard.
    //
    // I note this so verbosely because I have no other way to vent.
    if (typeof(tenantName) !== 'string') {
      throw new Error('tenantName must be a string.');
    }

    const tenant = await this.tenants.findOne({ name: tenantName });
    return tenant || null;
  }

  async getTenantByNameOrThrow(tenantName: string): Promise<Tenant> {
    const tenant = await this.getTenantByName(tenantName);

    if (!tenant) {
      throw new TenantNotFoundException();
    }

    return tenant;
  }

  async listTenantsByName(filter: string = '%'): Promise<Array<Tenant>> {
    const sqlFilter = filter.replace('*', '%');

    return this.tenants.find({ where: { name: Like(sqlFilter) }});
  }

  async withTenant<T>(tenantName: string, fn: (tenant: Tenant) => T | Promise<T>): Promise<T> {
    const tenant = await this.getTenantByName(tenantName);
    if (!tenant) {
      throw new TenantNotFoundException();
    }

    return fn(tenant);
  }
}
