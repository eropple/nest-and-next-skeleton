#! /usr/bin/env ts-node

import {CLI, Shim} from 'clime';
import * as Path from 'path';

import { ENV_PREFIX } from '../_cfg/_base';

process.env[`${ENV_PREFIX}_IS_CLI`] = '1';

// tslint:disable-next-line: no-floating-promises
(async () => {
  const cli = new CLI('backend-cli', Path.join(__dirname, 'commands'));

  const shim = new Shim(cli);
  await shim.execute(process.argv);

  process.exit(0);
})();
