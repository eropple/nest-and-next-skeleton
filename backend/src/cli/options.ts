import { option, Options } from 'clime';

export class BaseOptions extends Options {
  @option({
    flag: 'v',
    description: 'sets logger to DEBUG',
  })
  verbose!: boolean;
}
