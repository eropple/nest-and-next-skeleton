export const description = 'Commands related to cryptography.';

// Used when listing as subcommands.
export const brief = 'Commands for cryptography and key generation.';
