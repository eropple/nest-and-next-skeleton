import { command, Command, metadata, option, param } from 'clime';
import * as readline from 'readline-sync';

import { buildCli } from '../../../app-initialization';
import { TenantState } from '../../../directory/db/Tenant.entity';
import { TenantService } from '../../../directory/tenant.service';
import { BaseOptions } from '../../options';

export class ListTenantsCommandOptions extends BaseOptions {
  @option({
    type: String,
    required: false,
    description: 'Glob(-ish) filter for tenant names.',
  })
  filter!: string;
}

@command()
export default class ListTenantsCommand extends Command {
  @metadata
  async execute(
    options: ListTenantsCommandOptions,
  ) {
    const [logger, ctx] = await buildCli();
    const tenantService = ctx.get(TenantService);

    const tenants = await tenantService.listTenantsByName(options.filter);

    for (const tenant of tenants) {
      process.stdout.write(`${tenant.name}\n`);
    }
  }
}
