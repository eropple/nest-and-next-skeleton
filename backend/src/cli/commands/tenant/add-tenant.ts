import { command, Command, metadata, option, param } from 'clime';
import * as readline from 'readline-sync';

import { buildCli } from '../../../app-initialization';
import { TenantState } from '../../../directory/db/Tenant.entity';
import { TenantService } from '../../../directory/tenant.service';
import { BaseOptions } from '../../options';

export class AddTenantCommandOptions extends BaseOptions {
  @option({ type: String })
  displayName?: string;
}

@command()
export default class AddTenantCommand extends Command {
  @metadata
  async execute(
    @param({
      required: true,
    })
    name: string,
    options: AddTenantCommandOptions,
  ) {
    const [logger, ctx] = await buildCli();

    process.stderr.write('!!!! YOU ARE ADDING A TENANT !!!!\n');
    process.stderr.write(`NAME: ${name}\n`);
    process.stderr.write(`DISPLAY NAME: ${options.displayName || name}\n`);
    const mollyguard = readline.prompt({ prompt: 'Type YES if this is correct: ', history: false });

    if (mollyguard !== 'YES') {
      process.exit(1);
    }

    const tenantService = ctx.get(TenantService);

    const tenant = await tenantService.createTenant({
      name,
      displayName: options.displayName,
    });

    process.stdout.write(JSON.stringify(tenant, null, 2));
  }
}
