import { command, Command, metadata, option, param } from 'clime';

import { buildApp, buildOpenapiDocument } from '../../app-initialization';
import { BaseOptions } from '../options';

export class OpenapiDocCommandOptions extends BaseOptions {
}

@command()
export default class OpenapiDocCommand extends Command {
  @metadata
  async execute(
    options: OpenapiDocCommandOptions,
  ) {
    const app = await buildApp();
    const openapiJson = JSON.stringify(await buildOpenapiDocument(app), null, 2);

    process.stdout.write(`${openapiJson}\n`);
  }
}
