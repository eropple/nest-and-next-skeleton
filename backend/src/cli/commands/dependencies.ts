export const description = 'Dependency commands';

// Used when listing as subcommands.
export const brief = 'commands for viewing and analyzing dependencies';
