import { command, Command, metadata, option, param } from 'clime';

import { buildCli } from '../../../app-initialization';
import {
  generateSymmetricKeyB64,
  UNSAFE_FOR_PROD__generateSymmetricDevKeyB64,
} from '../../../utils/crypt';
import { BaseOptions } from '../../options';

export class GenerateSymmetricKeyCommandOptions extends BaseOptions {
  @option({
    description: 'If true, emits an all-zeroes key FOR DEV ONLY.',
    toggle: true,
  })
  devKey?: boolean;
}

@command()
export default class GenerateSymmetricKeyCommand extends Command {
  @metadata
  async execute(
    options: GenerateSymmetricKeyCommandOptions,
  ) {
    const [logger, ctx] = await buildCli();

    const key =
      options.devKey
        ? UNSAFE_FOR_PROD__generateSymmetricDevKeyB64()
        : generateSymmetricKeyB64();

    if (options.devKey) {
      logger.warn('!!! DO NOT USE THIS KEY IN A PRODUCTION CAPACITY. !!!');
    }

    process.stdout.write(`${key}\n`);
  }
}
