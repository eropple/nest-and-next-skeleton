import { command, Command, metadata, param } from 'clime';
import * as readline from 'readline-sync';

import { buildCli } from '../../../app-initialization';
import { UserService } from '../../../directory/user.service';
import { BaseOptions } from '../../options';

@command()
export default class ResetPasswordCommand extends Command {
  @metadata
  async execute(
    @param({
      required: true,
      description: 'tenant name',
    })
    tenantName: string,
    @param({
      required: true,
      description: 'username',
    })
    username: string,
    options: BaseOptions,
  ) {
    const [logger, ctx] = await buildCli();
    const userActions = await (ctx.get(UserService).forUserInTenant(tenantName, username));

    process.stdout.write(JSON.stringify(await userActions.resetPassword(), null, 2));
  }
}
