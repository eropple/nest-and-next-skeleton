import { command, Command, metadata, param } from 'clime';
import * as readline from 'readline-sync';

import { buildCli } from '../../../app-initialization';
import { UserService } from '../../../directory/user.service';
import { BaseOptions } from '../../options';

@command()
export default class SetPasswordCommand extends Command {
  @metadata
  async execute(
    @param({
      required: true,
      description: 'tenant name',
    })
    tenantName: string,
    @param({
      required: true,
      description: 'username',
    })
    username: string,
    options: BaseOptions,
  ) {
    const [logger, ctx] = await buildCli();
    const userActions = await (ctx.get(UserService).forUserInTenant(tenantName, username));

    const newPassword = readline.prompt({ prompt: 'New password (first):', history: false, hideEchoBack: true });
    const newPasswordAgain = readline.prompt({ prompt: 'New password (again):', history: false, hideEchoBack: true });

    if (newPassword !== newPasswordAgain) {
      throw new Error('Passwords don\'t match.');
    }

    process.stdout.write(JSON.stringify(await userActions.setPassword(newPassword), null, 2));
  }
}
