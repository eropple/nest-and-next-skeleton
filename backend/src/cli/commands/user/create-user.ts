import { command, Command, metadata, option, param } from 'clime';

import { buildCli } from '../../../app-initialization';
import { UserService } from '../../../directory/user.service';
import { BaseOptions } from '../../options';

export class CreateUserCommandOptions extends BaseOptions {
  @option({
    flag: 'P',
    toggle: true,
    description: 'User does not get a password. (If unset, will set a random password.)',
  })
  noPassword?: boolean;
  @option({
    flag: 'p',
    description: 'A password to hardcode. (Be careful with your command history!)',
  })
  password?: string;
}

@command()
export default class CreateUserCommand extends Command {
  @metadata
  async execute(
    @param({
      required: true,
      description: 'tenant name',
    })
    tenantName: string,
    @param({
      required: true,
      description: 'username',
    })
    username: string,
    @param({
      required: true,
      description: 'email',
    })
    email: string,
    options: CreateUserCommandOptions,
  ) {
    const [baseLogger, ctx] = await buildCli();
    const userServiceForTenant = await (ctx.get(UserService).forTenant(tenantName));

    const [newUser] = await userServiceForTenant.createUser(username, email);
    const logger = baseLogger.child({ username: newUser.name, email: newUser.email });
    logger.info(`Created new user ${newUser.name} (${newUser.email}).`);

    const userServiceForTenantInUser = await userServiceForTenant.forUser(newUser.name);

    let password: string | undefined;

    if (options.noPassword) {
      logger.info('New user does not have a password.');
    } else {
      const [user, newPassword] =
        await (options.password
          ? userServiceForTenantInUser.setPassword(options.password)
          : userServiceForTenantInUser.resetPassword());

      password = newPassword;
    }

    process.stdout.write(JSON.stringify({
      username: newUser.name,
      email: newUser.email,
      password,
    }, null, 2));
  }
}
