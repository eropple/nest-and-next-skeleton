import * as OAS from '@eropple/nestjs-openapi3';

@OAS.Model()
export class OkResponse {
  @OAS.Prop()
  readonly ok: boolean = true;
}
