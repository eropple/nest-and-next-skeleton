import { LogLevelString } from 'bunyan';
import { Duration } from 'luxon';

export interface SessionConfig {
  logLevel: LogLevelString;
  tokenPrefix: string;
  loginTokenLifespan: Duration;
  insecureTokenCookie: boolean;
  noHttpOnlyCookie: boolean;
}
