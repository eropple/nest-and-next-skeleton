import * as OAS from '@eropple/nestjs-openapi3';

import { UserPrivate } from '../directory/domain/UserPrivate.domain';

@OAS.Model()
export class LoginUnPwRequest {
  @OAS.Prop()
  readonly identity!: string;

  @OAS.Prop()
  readonly passwordPlaintext!: string;

  @OAS.Prop({}, { required: false })
  readonly totpCode?: string;
}

@OAS.Model()
export class LoginUnPwSuccessfulResponse {
  @OAS.Prop({ type: 'boolean', default: true })
  readonly success!: true;

  @OAS.Prop()
  readonly token!: string;

  @OAS.Prop()
  readonly user!: UserPrivate;
}

@OAS.Model()
export class LoginUnPwFailedResponse {
  @OAS.Prop({ type: 'boolean', default: false })
  readonly success!: false;

  @OAS.Prop({ type: 'string', enum: ['bad-credentials', 'totp-required', 'bad-totp'] })
  readonly reason!: 'bad-credentials' | 'totp-required' | 'bad-totp';
}

@OAS.Model()
export class RevokeTokenRequest {
  @OAS.Prop()
  readonly tokenSalt!: string;
}
