import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { User } from '../../directory/db/User.entity';

export enum AccessTokenType {
  LOGIN = 'login',
}
export const AccessTokenTypeValues: ReadonlyArray<AccessTokenType> = [AccessTokenType.LOGIN];

const accessTokenNames: Array<[AccessTokenType, string]> = [
  [AccessTokenType.LOGIN, 'LOGIN'],
];

export const AccessTokenToNames: ReadonlyMap<AccessTokenType, string> = new Map(accessTokenNames);
export const NamesToAccessToken: ReadonlyMap<string, AccessTokenType> = new Map(
  accessTokenNames.map(t => [t[1], t[0]]),
);

@Entity({ name: 'access_tokens' })
export class AccessToken {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @ManyToOne(t => User, { eager: true })
  @JoinColumn()
  readonly user!: User;

  @Column({ type: 'enum', enum: AccessTokenTypeValues })
  readonly type!: AccessTokenType;

  @Column({ type: 'text', unique: true })
  readonly salt!: string;

  @Column({ type: 'text' })
  readonly hash!: string;

  @Column({ type: 'text', array: true })
  readonly grants!: Array<string>;

  @CreateDateColumn()
  readonly createdAt!: Date;

  @Column({ type: 'timestamptz' })
  readonly expiresAt!: Date;

  @Column({ type: 'timestamptz', nullable: true })
  revokedAt!: Date | null;
}
