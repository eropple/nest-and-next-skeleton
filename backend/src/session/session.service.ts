import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import { Inject, Injectable, Scope } from '@nestjs/common';
import Argon2 from 'argon2';
import { DateTime, Duration } from 'luxon';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tenant } from '../directory/db/Tenant.entity';
import { User } from '../directory/db/User.entity';
import { UserPasswordScheme } from '../directory/db/UserPassword.entity';
import { TenantService } from '../directory/tenant.service';
import { UserService } from '../directory/user.service';
import { randomAlphanumericString, sha256String, timingSafeStringEquals } from '../utils/crypt';
import { SessionConfig } from './config';
import { AccessToken, AccessTokenToNames, AccessTokenType, NamesToAccessToken } from './db/AccessToken.entity';

export type LoginAttempt =
  { success: true, token: string, user: User } |
  { success: false, reason: 'bad-credentials' | 'totp-required' | 'bad-totp' };

@Injectable({ scope: Scope.REQUEST })
export class SessionService {
  private readonly logger: Bunyan;

  constructor(
    @Logger() logger: Bunyan,
    @Inject('SessionConfig') private readonly config: SessionConfig,
    @InjectRepository(AccessToken) private readonly accessTokens: Repository<AccessToken>,
    private readonly users: UserService,
    private readonly tenants: TenantService,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
    this.logger.level(this.config.logLevel);
  }

  async forTenant(tenant: string | Tenant): Promise<SessionServiceForTenant> {
    const t = tenant instanceof Tenant ? tenant : await this.tenants.getTenantByNameOrThrow(tenant);

    return new SessionServiceForTenant(
      this.logger,
      this.accessTokens,
      t,
      this.config,
      this.users,
    );
  }

  async validateToken(tokenString: string): Promise<AccessToken | null> {
    const tokenParts = splitToken(tokenString);
    // Bad token parse? Out.
    if (!tokenParts) {
      this.logger.debug('No token parts - null received from splitToken.');
      return null;
    }

    const token = await this.accessTokens.findOne({ salt: tokenParts.salt });
    // No token? Out.
    if (!token) {
      this.logger.debug('Token with salt not found in database.');
      return null;
    }

    const computedHash = sha256String(token.salt + tokenParts.key);
    if (!timingSafeStringEquals(token.hash, computedHash)) {
      this.logger.debug('Token hash mismatch.');
      return null;
    }

    return token;
  }
}

export class SessionServiceForTenant {
  private readonly logger: Bunyan;

  constructor(
    logger: Bunyan,
    private readonly accessTokens: Repository<AccessToken>,
    private readonly tenant: Tenant,
    private readonly config: SessionConfig,
    private readonly users: UserService,
  ) {
    this.logger = logger.child({ component: this.constructor.name, tenant: tenant.name });
  }

  async performLogin(
    identity: string,
    passwordPlaintext: string,
    totpCode?: string,
  ): Promise<LoginAttempt> {
    const usersForTenant = await this.users.forUserInTenant(this.tenant, identity);
    const user = usersForTenant.user;
    const logger = this.logger.child({ username: user.name });

    const { password, totp } = user;

    if (!password) {
      logger.info({ failed: true, cause: 'no-password' },
        'User attempted to log in, but this user lacks a local password.');
      return { success: false, reason: 'bad-credentials' };
    }

    let correct = false;
    switch (password.scheme) {
      case UserPasswordScheme.ARGON2:
        logger.debug('Argon2 password; checking.');
        correct = await Argon2.verify(password.hash, passwordPlaintext);
        break;
    }

    if (!correct) {
      logger.info({ failed: true, cause: 'invalid-password' }, 'Bad login.');
      return { success: false, reason: 'bad-credentials' };
    }

    // If a TOTP isn't provided, we need to communicate that back out.
    if (totp && totp.validated) {
      if (!totpCode) {
        logger.info({ failed: true, cause: 'totp-required' }, 'No TOTP provided.');
        return { success: false, reason: 'totp-required' };
      }

      const isTotpValid = await usersForTenant.checkTOTP(totpCode);
      if (!isTotpValid) {
        logger.info({ failed: true, cause: 'bad-totp' }, 'TOTP mismatch.');
        return { success: false, reason: 'bad-totp' };
      }
    }

    logger.debug('User logged in with local password. Creating session.');

    const token = await this.createToken(
      user,
      AccessTokenType.LOGIN,
      ['**/*'],
      this.config.loginTokenLifespan,
    );
    return { success: true, user: usersForTenant.user, token };
  }

  private async createToken(
    user: User,
    type: AccessTokenType,
    grants: Array<string>,
    duration: Duration,
  ): Promise<string> {
    const logger = this.logger.child({ username: user.name });

    const salt = randomAlphanumericString(16);
    const key = randomAlphanumericString(64);
    const hash = sha256String(salt + key);

    logger.debug('Generating token.');
    // We do include a `V1` if you in the future want to version your tokens.
    // I've had to do so occasionally, but it's not common.
    const token = `${this.config.tokenPrefix}_${AccessTokenToNames.get(type)}_V1_${salt}_${key}`;

    const newToken = await this.accessTokens.save({
      user,
      type,
      salt,
      hash,
      grants,
      expiresAt: DateTime.utc().plus(duration),
    });

    logger.info({ tokenType: type, newTokenSalt: salt }, 'New token generated.');

    return token;
  }
}

function splitToken(token: string) {
  const [prefix, type, version, salt, key] = token.split('_');

  if (!prefix || !type || !version || !salt || !key) {
    return null;
  }

  return { prefix, type, version, salt, key };
}
