import { AuthnDisallowed, AuthzScope } from '@eropple/nestjs-auth';
import { Bunyan, Logger } from '@eropple/nestjs-bunyan';
import * as OAS from '@eropple/nestjs-openapi3';
import { BadRequestException, Body, Controller, HttpCode, Inject, Response } from '@nestjs/common';
import { Response as ExpressResponse } from 'express';

import { USER_COOKIE_NAME } from '../authx/constants';
import { Tenant } from '../directory/db/Tenant.entity';
import { UserPrivate } from '../directory/domain/UserPrivate.domain';
import { UserNotFoundException } from '../directory/errors';
import { ReqTenant } from '../directory/tenant.decorator';
import { OkResponse } from '../domain';
import { SessionConfig } from './config';
import {
  LoginUnPwFailedResponse,
  LoginUnPwRequest,
  LoginUnPwSuccessfulResponse,
  RevokeTokenRequest,
} from './domain';
import { SessionService } from './session.service';

@Controller({ path: ':tenantName/session' })
@OAS.Tags('sessions')
@OAS.Parameter({
  name: 'tenantName',
  in: 'path',
  required: true,
  schema: { type: 'string' },
})
export class SessionController {
  private readonly logger: Bunyan;

  constructor(
    @Logger() logger: Bunyan,
    @Inject('SessionConfig') private readonly config: SessionConfig,
    private readonly sessions: SessionService,
  ) {
    this.logger = logger.child({ component: this.constructor.name });
  }

  @AuthnDisallowed()
  @AuthzScope('always')
  @HttpCode(200)
  @OAS.Post('login/unpw', {
    summary: 'Login using username/password + optional TOTP',
    responses: {
      200: { content: LoginUnPwSuccessfulResponse },
      403: {
        description: 'The user\'s credentials were incorrect. It might be because a TOTP code is required.',
        content: LoginUnPwFailedResponse,
      },
    },
  })
  async loginWithUnPw(
    @Response() res: ExpressResponse,
    @ReqTenant() tenant: Tenant | undefined,
    @OAS.Body() request: LoginUnPwRequest,
  ): Promise<void> {
    // if (!tenant) {
    //   throw new BadRequestException('tenant must be specified.');
    // }

    try {
      const loginAttempt = await (await this.sessions.forTenant(tenant!)).performLogin(
        request.identity,
        request.passwordPlaintext,
        request.totpCode,
      );

      const ret: LoginUnPwSuccessfulResponse | LoginUnPwFailedResponse =
        loginAttempt.success
          ? { success: true, token: loginAttempt.token, user: UserPrivate.fromUser(loginAttempt.user) }
          : { success: false, reason: loginAttempt.reason };

      if (ret.success) {
        res.cookie(USER_COOKIE_NAME, ret.token, {
          secure: !!(this.config.insecureTokenCookie),
          httpOnly: !!(this.config.noHttpOnlyCookie),
        });
      }

      res.statusCode = loginAttempt.success ? 200 : 403;
      res.json(ret);
    } catch (err) {
      // We don't want to reveal the existence of a user based on a bad identity.
      if (err instanceof UserNotFoundException) {
        const ret: LoginUnPwFailedResponse = { success: false, reason: 'bad-credentials' };
        res.statusCode = 403;
        res.json(ret);
      } else {
        throw err;
      }
    }
  }

  @AuthzScope('always')
  @HttpCode(200)
  @OAS.Post('revoke', {
    summary: 'Revokes an existing login or API token, rendering it unusable.',
    responses: {
      200: {
        description: 'The token has been successfully revoked.',
        content: OkResponse,
      },
    },
  })
  @OAS.SecurityScheme('token', [])
  async revoke(@Body() request: RevokeTokenRequest) {}
}
