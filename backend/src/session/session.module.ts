import { Module } from '@nestjs/common';
import { FactoryProvider } from '@nestjs/common/interfaces';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConfigService } from '../config/config.service';
import { DirectoryModule } from '../directory/directory.module';
import { AccessToken } from './db/AccessToken.entity';
import { SessionController } from './session.controller';
import { SessionService } from './session.service';

const config: FactoryProvider = {
  provide: 'SessionConfig',
  inject: [ConfigService],
  useFactory: (cfg: ConfigService) => cfg.session,
};

@Module({
  imports: [
    DirectoryModule,
    TypeOrmModule.forFeature([AccessToken]),
  ],
  providers: [config, SessionService],
  controllers: [SessionController],
  exports: [SessionService],
})
export class SessionModule {}
