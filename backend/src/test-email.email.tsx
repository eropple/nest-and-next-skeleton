import { Email } from '@eropple/nestjs-rx-mailer';
import React from 'react';

import { APPLICATION_NAME } from './_cfg/_base';

export interface TestEmailProps {
  title: string;
  text: string;
}

export class TestEmail extends React.Component<TestEmailProps> {
  render() {
    return (
      <Email
        title={this.props.title}
      >
        <p>
          This is a test email from {APPLICATION_NAME}. Important message below:
        </p>
        <p>
          {this.props.text}
        </p>
      </Email>
    );
  }
}
