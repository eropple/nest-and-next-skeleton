import {
  Store as ReduxStore,
} from 'redux';
import {
  createStore,
  Store as EasyStore,
} from 'easy-peasy';

import { appModel } from './models/app';

export const makeStore = (): ReduxStore => {
  const store: EasyStore = createStore(appModel);

  // This is a blind cast, but it's safe; easy-peasy guarantees that it creates
  // a standard Redux store even though its contract doesn't match in TypeScript.
  //
  // source: https://easy-peasy.now.sh/docs/quick-start.html#create-the-store
  return store as any;
}
