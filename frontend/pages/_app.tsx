import React from 'react';
import {
  default as NextApp,
  AppContext,
  AppInitialProps as NextAppInitialProps,
} from 'next/app';
import { StoreProvider, Store } from 'easy-peasy';
import withRedux from 'next-redux-wrapper';

import { makeStore } from '../redux/store';

export interface AppProps extends NextAppInitialProps {
  store?: Store;
}

class App extends NextApp<AppProps> {
  static async getInitialProps({ Component, ctx }: AppContext): Promise<AppProps> {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

    return { pageProps };

}

  render() {
    const { Component, pageProps, store } = this.props;

    if (!store) {
      throw new Error('Cannot render: no store.');
    }

    return (
      <StoreProvider store={store}>
        <Component {...pageProps} />
      </StoreProvider>
    );
  }
}

export default withRedux(makeStore)(App);
