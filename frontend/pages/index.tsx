import React from 'react';
import { NextPage } from 'next';

export interface IndexPageProps {}

const IndexPage: NextPage<IndexPageProps> = (props) =>
  <div>
    <span>hello world</span>
  </div>;

export default IndexPage;
